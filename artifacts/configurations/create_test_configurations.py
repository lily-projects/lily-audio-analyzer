"""
Create several test configurations
"""

from src.models.configuration import Configuration


open('empty_file.json', 'w').close()

config = Configuration()
config.save_to_file('empty_configuration.json')

config = Configuration()
config.update_measurement('Signal triangle 2kHz', {
    'type': 'Signal',
    'wave_form': 'triangle',
    'frequency': 2000,
    'amplitude': 70,
    'output_enable': True,
    'max_time': '2ms',
    'conversion_factor': 3,
    'max_amplitude': 2.5
})
config.update_measurement('Signal sine 500Hz', {
    'type': 'Signal',
    'wave_form': 'sine',
    'frequency': 500,
    'amplitude': 60,
    'output_enable': False,
    'max_time': '10ms',
    'conversion_factor': 2,
    'max_amplitude': 1.5
})
config.save_to_file('signal.json')

config = Configuration()
config.update_measurement('Signal', {'type': 'Signal'})
config.update_measurement('Spectrum', {'type': 'Spectrum analyzer'})
config.update_measurement('Frequency', {'type': 'Frequency response'})
config.update_measurement('Impedance', {'type': 'Impedance'})
config.save_to_file('all_measurements.json')
