"""
Script for starting the application.
"""

import wx

from src.app_init import APP_NAME
from src.app_init import VERSION
from src.controllers.controller_main import ControllerMain
from src.models.logger import Logger


def main():
    logger = Logger()
    logger.info('Start application')
    app = wx.App(redirect=False)
    controller = ControllerMain('{name} V{version}'.format(name=APP_NAME, version=VERSION), logger)
    controller.show_view()
    logger.info('Start main loop')
    app.MainLoop()
    logger.info('Application stopped')
    logger.shut_down()


if __name__ == '__main__':
    main()
