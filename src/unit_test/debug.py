"""
Various debug stuff
"""

import inspect

from datetime import datetime

_TIME_STAMP_FORMAT = "%Y%m%d %H:%M:%S.%f"


def debug(message):
    timestamp = datetime.now().strftime(_TIME_STAMP_FORMAT)
    calling_function = inspect.stack()[1].function
    print('{} - {} - {}'.format(timestamp, calling_function, message))


def _test_function():
    debug('Test message')


if __name__ == '__main__':

    _test_function()
