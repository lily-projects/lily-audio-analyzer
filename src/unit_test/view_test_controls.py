"""
View for testing the control boxes.
"""

import wx

from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls


class ViewTestControls(wx.Dialog):

    _GAP = 10

    def __init__(self):
        super().__init__(None, wx.ID_ANY, 'Controls generator test', size=(300, 600))

        panel = wx.Panel(self, wx.ID_ANY)
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(GeneratorControls(panel, GeneratorControls.SHOW_ALL, self._add_control_callback),
                0, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(MeasurementControls(panel, MeasurementControls.SHOW_ALL, self._add_control_callback),
                0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.BOTTOM, self._GAP)
        panel.SetSizer(box)

    @staticmethod
    def _add_control_callback(*params):
        print(params)


def show_view_test_controls():
    _app = wx.App(redirect=False)
    frame = ViewTestControls()
    frame.ShowModal()
    frame.Destroy()


if __name__ == '__main__':

    show_view_test_controls()
