"""
Shows a simple plot for analyzing data.
"""

import wx

from src.models.plot_data import PlotData
from src.views.view_plot import ViewPlot


def view_simple_plot(x_values, y_values, y_min=0.0, y_max=0.0):

    class ViewSimplePlot(wx.Frame):
        _GAP = 10
        _MIN_SIZE = (900, 600)

        def __init__(self, plot_data):
            super().__init__(None, wx.ID_ANY, 'Plot view')
            panel = wx.Panel(self, wx.ID_ANY)

            plot = ViewPlot(panel)
            box = wx.BoxSizer(wx.VERTICAL)
            box.Add(plot, 1, wx.EXPAND | wx.ALL, self._GAP)
            panel.SetSizer(box)

            self.SetInitialSize(self._MIN_SIZE)

            wx.CallAfter(plot.plot, plot_data)

    _plot_data = PlotData()
    _plot_data.set_x_data(x_values)
    _plot_data.set_y_data(y_values)
    if y_min < y_max:
        _plot_data.set_y_min(y_min)
        _plot_data.set_y_max(y_max)

    app = wx.App(redirect=False)
    frame = ViewSimplePlot(_plot_data)
    frame.Show()
    app.MainLoop()


if __name__ == '__main__':

    import numpy

    n_samples = 250
    sample_rate = 48000
    frequency = 1000

    t_values = numpy.arange(n_samples) / sample_rate
    a_values = numpy.sin(2 * numpy.pi * frequency * t_values)

    view_simple_plot(t_values, [a_values], -1.1, 1.1)
