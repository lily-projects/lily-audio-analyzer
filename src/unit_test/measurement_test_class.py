"""
Measurement test class.
"""

from src.measurements.measurement_base import MeasurementBase
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls


class MeasurementBaseTest(MeasurementBase):

    def __init__(self, setting_change_callback):
        name = 'Measurement base class test'
        generator_options = GeneratorControls.SHOW_ALL
        measurement_options = MeasurementControls.SHOW_ALL
        super().__init__(name, generator_options, measurement_options, setting_change_callback)


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementBaseTest)
