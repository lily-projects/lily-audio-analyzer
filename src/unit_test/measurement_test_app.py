"""
Test application for testing a measurement.
"""

import wx

from src.models.application_settings import ApplicationSettings
from src.models.devices import get_list_of_input_device_names
from src.models.devices import get_list_of_output_device_names
from src.models.logger import Logger
from src.views.view_dialogs import show_message
from src.views.view_plot import ViewPlot


class ViewMeasurementTestApp(wx.Frame):

    _GAP = 5
    _MIN_SIZE = (1200, 800)
    _INIT_POS = (60, 10)
    _INITIAL_UPDATE_INTERVAL = 1000

    def __init__(self, measurement_class, logger):
        super().__init__(None, wx.ID_ANY, 'Measurement Test')
        self._app_settings = ApplicationSettings(logger)
        self._measurement = measurement_class(self._on_setting_change)

        panel = wx.Panel(self, wx.ID_ANY)

        inner_box = wx.BoxSizer(wx.HORIZONTAL)
        inner_box.Add(self._create_measurement_box(panel), 0, wx.EXPAND)
        inner_box.Add(self._create_plot_box(panel), 1, wx.EXPAND | wx.LEFT, self._GAP)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._create_devices_box(panel), 0, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(inner_box, 1, wx.EXPAND | wx.ALL, self._GAP)

        panel.SetSizer(box)

        self.Bind(wx.EVT_CLOSE, self._on_app_close)

        self.SetInitialSize(self._MIN_SIZE)
        self.SetPosition(self._INIT_POS)

        self._update_timer = wx.Timer(self, wx.ID_ANY)
        self.Bind(wx.EVT_TIMER, self._on_update_timer, self._update_timer)
        self._update_timer.Start(self._INITIAL_UPDATE_INTERVAL)

    ###########
    # Private #
    ###########

    def _create_devices_box(self, parent):
        lbl_output_device = wx.StaticText(parent, wx.ID_ANY, 'Output device:')
        lbl_input_device = wx.StaticText(parent, wx.ID_ANY, 'Input device:')

        output_devices = get_list_of_output_device_names(True)
        self._cmb_output_device = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY)
        self._cmb_output_device.SetItems(output_devices)
        self._cmb_output_device.SetValue(self._app_settings.get_output_device_name())

        input_devices = get_list_of_input_device_names(True)
        self._cmb_input_device = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY)
        self._cmb_input_device.SetItems(input_devices)
        self._cmb_input_device.SetValue(self._app_settings.get_input_device_name())

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(lbl_output_device, (0, 0), wx.DefaultSpan)
        grid.Add(self._cmb_output_device, (0, 1), wx.DefaultSpan, wx.EXPAND)
        grid.Add(lbl_input_device, (1, 0), wx.DefaultSpan)
        grid.Add(self._cmb_input_device, (1, 1), wx.DefaultSpan, wx.EXPAND)
        grid.AddGrowableCol(1)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Devices: '), wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._GAP)

        return box

    def _create_measurement_box(self, parent):
        lbl_name_caption = wx.StaticText(parent, wx.ID_ANY, 'Name:')
        lbl_name_value = wx.StaticText(parent, wx.ID_ANY, self._measurement.get_name())

        measurement_panel = self._measurement.get_controls_panel(parent)

        btn_apply_settings = wx.Button(parent, wx.ID_ANY, 'Change Settings')
        btn_apply_settings.Bind(wx.EVT_BUTTON, self._on_change_settings_button)
        self._btn_run_measurement = wx.Button(parent, wx.ID_ANY, 'Run')
        self._btn_run_measurement.Bind(wx.EVT_BUTTON, self._on_run_button)

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(lbl_name_caption, (0, 0), wx.DefaultSpan)
        grid.Add(lbl_name_value, (0, 1), wx.DefaultSpan, wx.EXPAND)
        grid.AddGrowableCol(1)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Measurement: '), wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(measurement_panel, 1, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(btn_apply_settings, 0, wx.ALL, self._GAP)
        box.Add(self._btn_run_measurement, 0, wx.ALL, self._GAP)

        return box

    def _create_plot_box(self, parent):
        self._plot = ViewPlot(parent)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Graph: '), wx.VERTICAL)
        box.Add(self._plot, 1, wx.EXPAND | wx.ALL, self._GAP)

        return box

    def _set_update_timer_interval(self):
        self._update_timer.Stop()
        self._update_timer.Start(self._measurement.get_plot_update_interval())

    ##################
    # Event handlers #
    ##################

    def _on_update_timer(self, event):
        if self._measurement.is_running():
            self._btn_run_measurement.SetLabel('Stop')
            self._plot.plot(self._measurement.get_plot_data())
            self._set_update_timer_interval()
        else:
            self._btn_run_measurement.SetLabel('Run')

        event.Skip()

    def _on_setting_change(self):
        print(self._measurement.get_settings())

    def _on_change_settings_button(self, event):
        settings = self._measurement.get_settings()
        for key in settings.keys():
            with wx.TextEntryDialog(self, 'Enter a value for {key}'.format(key=key),
                                    caption='Change setting', value=str(settings[key])) as dlg:
                if dlg.ShowModal() == wx.ID_OK:
                    settings[key] = dlg.GetValue()
                else:
                    break
        self._measurement.apply_settings(settings)
        event.Skip()

    def _on_run_button(self, event):
        if self._measurement.is_running():
            self._measurement.stop()
            self._btn_run_measurement.SetLabel('Run')
        else:
            output_device_name = self._cmb_output_device.GetValue()
            if output_device_name == '':
                show_message(self, 'Run measurement', 'Select an output device.', wx.ICON_INFORMATION)
            input_device_name = self._cmb_input_device.GetValue()
            if output_device_name != '' and input_device_name == '':
                show_message(self, 'Run measurement', 'Select an input device.', wx.ICON_INFORMATION)

            if output_device_name != '' and input_device_name != '':
                self._measurement.set_output_device_name(output_device_name)
                self._measurement.set_input_device_name(input_device_name)
                self._measurement.run()
                self._set_update_timer_interval()
                self._btn_run_measurement.SetLabel('Stop')

        event.Skip()

    def _on_app_close(self, event):
        self._measurement.stop()

        device_name = self._cmb_input_device.GetValue()
        if device_name != '':
            self._app_settings.store_input_device_name(device_name)
        device_name = self._cmb_output_device.GetValue()
        if device_name != '':
            self._app_settings.store_output_device_name(device_name)

        event.Skip()


def test_measurement(measurement_class):
    test_logger = Logger(True)

    app = wx.App(redirect=False)

    view = ViewMeasurementTestApp(measurement_class, test_logger)
    view.Show()

    app.MainLoop()


if __name__ == '__main__':

    from src.unit_test.measurement_test_class import MeasurementBaseTest

    test_measurement(MeasurementBaseTest)
