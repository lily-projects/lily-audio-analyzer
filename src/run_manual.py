"""
Run the application with a fixed window size and position, so all creanshots in the manual are identical.
"""

from src.main import main
from src.models.application_settings import ApplicationSettings


settings = ApplicationSettings()

settings.store_main_window_maximized(False)
settings.store_main_window_position((100, 50))
settings.store_main_window_size((1920, 1080))

settings.store_log_window_auto_open(True)
settings.store_log_window_maximized(False)
settings.store_log_window_position((2030, 50))
settings.store_log_window_size((800, 1080))

settings.store_input_device_name('Microphone (2- Scarlett 2i2 USB), 2 channels, 96000Hz, Windows WASAPI')
settings.store_output_device_name('Speakers (2- Scarlett 2i2 USB), 2 channels, 96000Hz, Windows WASAPI')

main()
