"""
List of available measurements
"""

from src.measurements.measurement_frequency_response import MeasurementFrequencyResponse
from src.measurements.measurement_impedance import MeasurementImpedance
from src.measurements.measurement_signal import MeasurementSignal
from src.measurements.measurement_spectrum_analyzer import MeasurementSpectrumAnalyzer


LIST_OF_MEASUREMENTS = [
    MeasurementSignal,
    MeasurementSpectrumAnalyzer,
    MeasurementFrequencyResponse,
    MeasurementImpedance
]


def get_measurement_names():
    return list(map(lambda x: x(print).get_name(), LIST_OF_MEASUREMENTS))


def get_measurement_by_name(measurement_name):
    matches = list(filter(lambda x: x(print).get_name() == measurement_name, LIST_OF_MEASUREMENTS))
    if len(matches) == 1:
        return matches[0]

    return None


if __name__ == '__main__':

    measurement_names = get_measurement_names()
    print(measurement_names)

    print(get_measurement_by_name(measurement_names[0]))
    print(get_measurement_by_name('Not existing measurement'))
