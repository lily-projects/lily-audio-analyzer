"""
Measurement frequency response.
Makes a frequency sweep on the output and measures the amplitude on the input.
"""

import math
import numpy
import threading
import time
import wx

from src.measurements.measurement_base import MeasurementBase
from src.models.plot_data import PlotData
from src.models.plot_data import PlotProperty
from src.models.string_formatter import format_value


class MeasurementFrequencyResponse(MeasurementBase):

    _NAME = 'Frequency response'

    _DEFAULT_PLOT_UPDATE_INTERVAL = 333
    _DEFAULT_CONVERSION_FACTOR = 1
    _DEFAULT_MAX_AMPLITUDE = '10'
    _DEFAULT_MIN_AMPLITUDE = '-80'
    _DEFAULT_START_FREQUENCY = 10
    _DEFAULT_STOP_FREQUENCY = 22000
    _N_POINTS = 15
    _LOOP_INTERVAL = 0.5
    _N_PERIODS = 4

    #############
    # Overrides #
    #############

    def _create_controls_box(self, parent):
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._create_generator_box(parent, False, False, False), 0, wx.EXPAND | wx.TOP, self._CONTROLS_SPACING)
        box.Add(self._create_frequency_response_box(parent), 0, wx.EXPAND | wx.TOP, self._CONTROLS_SPACING)
        return box

    def _run_measurement(self):
        self._measurement_data = PlotData()

        self._generator.set_wave_form(self._generator.WAVE_SINE)
        self._generator.set_amplitude(0)
        self._generator.start()
        self._grabber.start()

        t = threading.Thread(target=self._measure_frequency_response)
        t.daemon = True
        t.start()

    def _generate_plot_data(self):
        max_amplitude = self._DEFAULT_MAX_AMPLITUDE
        min_amplitude = self._DEFAULT_MIN_AMPLITUDE

        try:
            max_amplitude = int(self._cmb_max_amplitude.GetValue())
            min_amplitude = int(self._cmb_min_amplitude.GetValue())
        except (Exception, ):
            pass

        plot_data = self._measurement_data
        plot_data.X_LEGEND = 'frequency [Hz]'
        plot_data.Y_LEGEND = 'gain [dB]'
        plot_data.Y_MAX = max_amplitude
        plot_data.Y_MIN = min_amplitude

        if len(list(filter(lambda x: x.LABEL == 'Max latency', plot_data.PLOT_PROPERTIES))) == 0:
            max_input_latency = self._grabber.get_device().get_default_max_latency()
            max_output_latency = self._generator.get_device().get_default_max_latency()
            max_latency = max_input_latency + max_output_latency

            plot_property = PlotProperty()
            plot_property.GROUP = 'general'
            plot_property.LABEL = 'Max latency'
            plot_property.VALUE = format_value(max_latency, 's')
            plot_data.PLOT_PROPERTIES.append(plot_property)

        return plot_data

    ###########
    # Private #
    ###########

    def _create_frequency_response_box(self, parent):
        lbl_conversion = wx.StaticText(parent, wx.ID_ANY, 'Conversion factor:')
        self._txt_conversion = wx.TextCtrl(parent, wx.ID_ANY, str(self._DEFAULT_CONVERSION_FACTOR), size=(50, -1))
        self._add_settings_control('conversion_factor', float, self._txt_conversion, self._DEFAULT_CONVERSION_FACTOR)

        amplitude_values = [str(x) for x in range(40, -160, -10)]

        lbl_max_amplitude = wx.StaticText(parent, wx.ID_ANY, 'Max amplitude [dBV]:')
        self._cmb_max_amplitude = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY, choices=amplitude_values[:-1])
        self._cmb_max_amplitude.SetValue(self._DEFAULT_MAX_AMPLITUDE)
        self._add_settings_control('max_amplitude', float, self._cmb_max_amplitude, self._DEFAULT_MAX_AMPLITUDE)

        lbl_min_amplitude = wx.StaticText(parent, wx.ID_ANY, 'Min amplitude [dBV]:')
        self._cmb_min_amplitude = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY, choices=amplitude_values[1:])
        self._cmb_min_amplitude.SetValue(self._DEFAULT_MIN_AMPLITUDE)
        self._add_settings_control('min_amplitude', float, self._cmb_max_amplitude, self._DEFAULT_MIN_AMPLITUDE)

        lbl_start_frequency = wx.StaticText(parent, wx.ID_ANY, 'Start frequency [Hz]:')
        self._spin_start_frequency = wx.SpinCtrl(parent, wx.ID_ANY, min=10, max=100000,
                                                 initial=self._DEFAULT_START_FREQUENCY)
        self._add_settings_control('start_frequency', int, self._spin_start_frequency, self._DEFAULT_START_FREQUENCY)

        lbl_stop_frequency = wx.StaticText(parent, wx.ID_ANY, 'Stop frequency [Hz]:')
        self._spin_stop_frequency = wx.SpinCtrl(parent, wx.ID_ANY, min=10, max=100000,
                                                initial=self._DEFAULT_STOP_FREQUENCY)
        self._add_settings_control('stop_frequency', int, self._spin_stop_frequency, self._DEFAULT_STOP_FREQUENCY)

        grid = wx.GridBagSizer(self._CONTROLS_SPACING, self._CONTROLS_SPACING)
        grid.Add(lbl_conversion, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._txt_conversion, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_max_amplitude, (1, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._cmb_max_amplitude, (1, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_min_amplitude, (2, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._cmb_min_amplitude, (2, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_start_frequency, (3, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._spin_start_frequency, (3, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_stop_frequency, (4, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._spin_stop_frequency, (4, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Frequency response: '), wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._CONTROLS_SPACING)

        return box

    def _generate_frequency_list(self, start_frequency, stop_frequency):
        frequency_step = 1
        a = b = 1
        if self._measurement_data.X_LOG_SCALE:
            n_decades = math.ceil(math.log10(stop_frequency / start_frequency))
            n_points = n_decades * self._N_POINTS
            a = math.exp(((n_points + 1) * math.log(start_frequency) - math.log(stop_frequency)) / n_points)
            b = math.log(stop_frequency / start_frequency) / n_points
        else:
            n_points = self._N_POINTS * stop_frequency / start_frequency / 10
            frequency_step = (stop_frequency - start_frequency) / n_points

        i = 2
        frequencies = [start_frequency]
        while frequencies[-1] < stop_frequency:
            if self._measurement_data.X_LOG_SCALE:
                frequency = round(a * math.exp(b * i))
            else:
                frequency = round(start_frequency + (frequency_step * (i - 1)))
            if frequency not in frequencies:
                frequencies.append(frequency)
            i += 1

        return frequencies

    def _measure_frequency_response(self):
        sample_rate = self._grabber.get_device().get_default_sample_rate()
        n_channels = self._grabber.get_device().get_max_input_channels()
        max_input_latency = self._grabber.get_device().get_default_max_latency()
        max_output_latency = self._generator.get_device().get_default_max_latency()
        max_latency = (max_input_latency + max_output_latency) * 1.25

        while True:
            min_amplitude = self._DEFAULT_MIN_AMPLITUDE
            conversion_factor = self._DEFAULT_CONVERSION_FACTOR
            start_frequency = self._DEFAULT_START_FREQUENCY
            stop_frequency = self._DEFAULT_STOP_FREQUENCY
            try:
                conversion_factor = float(self._txt_conversion.GetValue())
                min_amplitude = int(self._cmb_min_amplitude.GetValue())
                start_frequency = int(self._spin_start_frequency.GetValue())
                stop_frequency = int(self._spin_stop_frequency.GetValue())
            except (Exception,):
                pass

            if stop_frequency <= start_frequency:
                stop_frequency = start_frequency * 10

            self._measurement_data.X_LOG_SCALE = stop_frequency / start_frequency >= 100
            self._measurement_data.X_DATA = self._generate_frequency_list(start_frequency, stop_frequency)
            if len(self._measurement_data.Y_DATA) == 0:
                self._measurement_data.Y_DATA = numpy.full((n_channels, len(self._measurement_data.X_DATA)),
                                                           min_amplitude)

            for frequency_index in range(len(self._measurement_data.X_DATA)):
                frequency = self._measurement_data.X_DATA[frequency_index]
                n_samples = math.ceil(self._N_PERIODS * sample_rate / frequency)
                self._grabber.set_max_samples(n_samples)

                self._generator.set_frequency(frequency)
                ref_amplitude = float(self._spin_amplitude.GetValue()) / 100
                self._generator.set_amplitude(ref_amplitude)

                grab_time = 1.5 * self._N_PERIODS / frequency
                time.sleep(max_latency + grab_time)
                data = self._grabber.get_data()
                if data is not None:
                    data *= conversion_factor
                    for i, channel_data in enumerate(data):
                        amplitude = self._calculate_peak_peak(channel_data) / 2
                        self._measurement_data.Y_DATA[i][frequency_index] = 20 * numpy.log10(amplitude / ref_amplitude)

                if not self.is_running():
                    break

            time.sleep(self._LOOP_INTERVAL)
            if not self.is_running():
                break


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementFrequencyResponse)
