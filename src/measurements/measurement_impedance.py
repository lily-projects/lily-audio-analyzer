"""
Measurement impedance.
Measures impedance over frequency.
Requires two input channels.

Test circuit:

                 |---------------------- in ch1
                 |
    out ch1 -----------| Rs |----------- in ch2
                                  |
                                 ---
                                 R?
                                 ---
                                  |
                                 GND

Calculate R?:
  I = U / R = (U_in1 - U_in2) / Rs
  R? = U / I = U_in2 / I
  R? = Rs * U_in2 / (U_in1 - U_in2)

"""

from src.measurements.measurement_base import MeasurementBase
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls


class MeasurementImpedance(MeasurementBase):

    def __init__(self, setting_change_callback):
        name = 'Impedance'
        generator_options = [
            GeneratorControls.SHOW_AMPLITUDE
        ]
        measurement_options = [
            MeasurementControls.SHOW_SENSE_RESISTOR,
            MeasurementControls.SHOW_IMPEDANCE,
            MeasurementControls.SHOW_START_STOP_FREQUENCY
        ]
        super().__init__(name, generator_options, measurement_options, setting_change_callback)


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementImpedance)
