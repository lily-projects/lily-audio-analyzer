"""
Measurement signal.
This measurement has a generator for generating a signal on the selected output.
The samples from the input are shown on the plot.
"""

import numpy

from src.measurements.measurement_base import MeasurementBase
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls
from src.models.string_formatter import format_value


class MeasurementSignal(MeasurementBase):

    _MAX_TIME_TO_VALUE = {
        'us': 1000000,
        'ms': 1000,
        's': 1
    }

    _MIN_PLOT_UPDATE_INTERVAL = 200

    def __init__(self, setting_change_callback):
        name = 'Signal'
        generator_options = GeneratorControls.SHOW_ALL
        measurement_options = [
            MeasurementControls.SHOW_MAX_TIME,
            MeasurementControls.SHOW_AMPLITUDE,
            MeasurementControls.SHOW_TRIGGER
        ]
        super().__init__(name, generator_options, measurement_options, setting_change_callback)

    ###########
    # Private #
    ###########

    def _get_max_time(self):
        max_time = self.get_value_from_control('max_time', MeasurementControls.DEFAULT_MAX_TIME)
        for key in self._MAX_TIME_TO_VALUE.keys():
            if key in max_time:
                max_time = float(max_time.replace(key, '')) / self._MAX_TIME_TO_VALUE[key]
                break
        return max_time

    def _set_grabber_max_samples(self):
        sample_rate = self.get_input_sample_rate()
        if sample_rate > 0:
            max_time = self._get_max_time()
            # We take two times more than we need for the rising edge triggering
            self.set_max_samples(int(2 * sample_rate * max_time))

    #########################
    # Overrides from parent #
    #########################

    def process_changed_settings(self):
        self._set_grabber_max_samples()

    def get_plot_update_interval(self):
        max_time = self._get_max_time()
        if max_time > self._MIN_PLOT_UPDATE_INTERVAL:
            return max_time

        return self._MIN_PLOT_UPDATE_INTERVAL

    def process_data(self, data):
        max_amplitude = self.get_value_from_control('max_amplitude', MeasurementControls.DEFAULT_MAX_AMPLITUDE)
        trigger_edge = self.get_value_from_control('trigger_edge', MeasurementControls.DEFAULT_TRIGGER_EDGE)
        trigger_level = self.get_value_from_control('trigger_level', MeasurementControls.DEFAULT_TRIGGER_LEVEL)
        sample_rate = self.get_input_sample_rate()
        max_time = self._get_max_time()

        # Take a few samples more to make sure we cover the complete time span
        n_samples = int(sample_rate * max_time) + 2
        x_data = numpy.arange(n_samples) / sample_rate

        # Trigger on rising edge
        start_rising_edge = 0
        for channel_data in data:
            rising_edges = self.get_tools().get_edges(channel_data, trigger_edge, trigger_level)
            if len(rising_edges) > 0:
                start_rising_edge = rising_edges[0]

        # Limit the Y data according to the position of the rising edge and the amount of samples
        y_data = []
        for i in range(len(data)):
            y_data.append(data[i][start_rising_edge:][:n_samples])

        self.update_plot_data(x_data, y_data, -max_amplitude, max_amplitude)

        for i, channel_data in enumerate(y_data):
            self.update_plot_property('channel {}'.format(i + 1), 'Peak-peak',
                                      format_value(self.get_tools().calculate_peak_peak(channel_data), 'V'))
            self.update_plot_property('channel {}'.format(i + 1), 'Mean',
                                      format_value(self.get_tools().calculate_mean(channel_data), 'V'))
            self.update_plot_property('channel {}'.format(i + 1), 'RMS',
                                      format_value(self.get_tools().calculate_rms(channel_data), 'V'))
            self.update_plot_property('channel {}'.format(i + 1), 'Frequency',
                                      format_value(self.get_tools().calculate_frequency(channel_data, x_data),
                                                   'Hz'))


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementSignal)
