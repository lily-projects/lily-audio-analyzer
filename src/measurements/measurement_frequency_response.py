"""
Measurement frequency response.
Makes a frequency sweep on the output and measures the amplitude on the input.
"""

from src.measurements.measurement_base import MeasurementBase
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls


class MeasurementFrequencyResponse(MeasurementBase):

    def __init__(self, setting_change_callback):
        name = 'Frequency response'
        generator_options = [
            GeneratorControls.SHOW_AMPLITUDE
        ]
        measurement_options = [
            MeasurementControls.SHOW_AMPLITUDE_DB,
            MeasurementControls.SHOW_START_STOP_FREQUENCY
        ]
        super().__init__(name, generator_options, measurement_options, setting_change_callback)


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementFrequencyResponse)
