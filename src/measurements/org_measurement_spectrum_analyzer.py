"""
Measurement spectrum analyzer.
Shows the spectrum of the input signal using FFT analysis.
"""

import math
import numpy
import wx

from src.measurements.measurement_base import MeasurementBase
from src.models.plot_data import PlotData
from src.models.plot_data import PlotProperty


class MeasurementSpectrumAnalyzer(MeasurementBase):

    _NAME = 'Spectrum analyzer'

    _DEFAULT_PLOT_UPDATE_INTERVAL = 333
    _DEFAULT_CONVERSION_FACTOR = 1
    _DEFAULT_START_FREQUENCY = 10
    _DEFAULT_STOP_FREQUENCY = 22000
    _AMPLITUDE_CORRECTION = numpy.pi
    _FREQ_WINDOW = 10

    #############
    # Overrides #
    #############

    _DEFAULT_MAX_AMPLITUDE = '0'
    _DEFAULT_MIN_AMPLITUDE = '-150'

    def _create_controls_box(self, parent):
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._create_generator_box(parent), 0, wx.EXPAND | wx.TOP, self._CONTROLS_SPACING)
        box.Add(self._create_spectrum_analyzer_box(parent), 0, wx.EXPAND | wx.TOP, self._CONTROLS_SPACING)
        return box

    def _run_measurement(self):
        sample_rate = self._grabber.get_device().get_default_sample_rate()
        n_samples = int(0.9 * sample_rate * self._DEFAULT_PLOT_UPDATE_INTERVAL / 1000)

        self._grabber.set_max_samples(n_samples)
        self._grabber.start()
        if self._chk_output.GetValue():
            self._start_generator()

    def _generate_plot_data(self):
        conversion_factor = self._DEFAULT_CONVERSION_FACTOR
        max_amplitude = self._DEFAULT_MAX_AMPLITUDE
        min_amplitude = self._DEFAULT_MIN_AMPLITUDE
        start_frequency = self._DEFAULT_START_FREQUENCY
        stop_frequency = self._DEFAULT_STOP_FREQUENCY

        try:
            conversion_factor = float(self._txt_conversion.GetValue())
            max_amplitude = int(self._cmb_max_amplitude.GetValue())
            min_amplitude = int(self._cmb_min_amplitude.GetValue())
            start_frequency = int(self._spin_start_frequency.GetValue())
            stop_frequency = int(self._spin_stop_frequency.GetValue())
        except (Exception, ):
            pass

        if min_amplitude >= max_amplitude:
            min_amplitude = max_amplitude - 10

        if stop_frequency <= start_frequency:
            stop_frequency = start_frequency + 100

        plot_data = PlotData()
        plot_data.X_LEGEND = 'frequency [Hz]'
        plot_data.Y_LEGEND = 'amplitude [dBV]'

        data = self._grabber.get_data()
        if data is not None:
            n_samples = len(data[0])
            sample_rate = self._grabber.get_device().get_default_sample_rate()

            x_data = self._calculate_fft_frequencies(n_samples, sample_rate)

            start_index = numpy.where(x_data >= start_frequency)[0][0]
            stop_index = numpy.where(x_data > stop_frequency)[0][0]
            plot_data.X_DATA = x_data[start_index:stop_index]
            plot_data.X_LOG_SCALE = self._chk_log_scale.GetValue()

            y_data = self._calculate_fft(data, conversion_factor, n_samples)

            plot_data.Y_DATA = []
            for i, channel_data in enumerate(y_data):
                channel_data = channel_data[start_index:stop_index]
                plot_data.Y_DATA.append(20 * numpy.log10(channel_data))

                result = self._analyze_fft_data(channel_data, plot_data.X_DATA)
                if result['thd_noise'] > 1:
                    result['thd_noise'] = '%.2f' % result['thd_noise']
                else:
                    result['thd_noise'] = '%.7f' % result['thd_noise']

                if result['thd'] > 1:
                    result['thd'] = '%.2f' % result['thd']
                else:
                    result['thd'] = '%.7f' % result['thd']

                plot_property = PlotProperty()
                plot_property.GROUP = 'channel {index}'.format(index=i + 1)
                plot_property.LABEL = 'Peak amplitude'
                plot_property.VALUE = '%.2f dBV' % result['peak']
                plot_data.PLOT_PROPERTIES.append(plot_property)

                plot_property = PlotProperty()
                plot_property.GROUP = 'channel {index}'.format(index=i + 1)
                plot_property.LABEL = 'Frequency @ peak'
                plot_property.VALUE = '%.0f Hz' % result['frequency_peak']
                plot_data.PLOT_PROPERTIES.append(plot_property)

                plot_property = PlotProperty()
                plot_property.GROUP = 'channel {index}'.format(index=i + 1)
                plot_property.LABEL = 'THD'
                plot_property.VALUE = '%s %%' % result['thd']
                plot_data.PLOT_PROPERTIES.append(plot_property)

                plot_property = PlotProperty()
                plot_property.GROUP = 'channel {index}'.format(index=i + 1)
                plot_property.LABEL = 'THD+N'
                plot_property.VALUE = '%s %%' % result['thd_noise']
                plot_data.PLOT_PROPERTIES.append(plot_property)

                plot_property = PlotProperty()
                plot_property.GROUP = 'channel {index}'.format(index=i + 1)
                plot_property.LABEL = 'SNR'
                plot_property.VALUE = '%.1f dB' % result['snr']
                plot_data.PLOT_PROPERTIES.append(plot_property)

            plot_data.Y_MAX = max_amplitude
            plot_data.Y_MIN = min_amplitude

        return plot_data

    ###########
    # Private #
    ###########

    def _create_spectrum_analyzer_box(self, parent):
        lbl_conversion = wx.StaticText(parent, wx.ID_ANY, 'Conversion factor:')
        self._txt_conversion = wx.TextCtrl(parent, wx.ID_ANY, str(self._DEFAULT_CONVERSION_FACTOR), size=(50, -1))
        self._add_settings_control('conversion_factor', float, self._txt_conversion, self._DEFAULT_CONVERSION_FACTOR)

        amplitude_values = [str(x) for x in range(40, -160, -10)]

        lbl_max_amplitude = wx.StaticText(parent, wx.ID_ANY, 'Max amplitude [dBV]:')
        self._cmb_max_amplitude = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY, choices=amplitude_values[:-1])
        self._cmb_max_amplitude.SetValue(self._DEFAULT_MAX_AMPLITUDE)
        self._add_settings_control('max_amplitude', float, self._cmb_max_amplitude, self._DEFAULT_MAX_AMPLITUDE)

        lbl_min_amplitude = wx.StaticText(parent, wx.ID_ANY, 'Min amplitude [dBV]:')
        self._cmb_min_amplitude = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY, choices=amplitude_values[1:])
        self._cmb_min_amplitude.SetValue(self._DEFAULT_MIN_AMPLITUDE)
        self._add_settings_control('min_amplitude', float, self._cmb_max_amplitude, self._DEFAULT_MIN_AMPLITUDE)

        lbl_start_frequency = wx.StaticText(parent, wx.ID_ANY, 'Start frequency [Hz]:')
        self._spin_start_frequency = wx.SpinCtrl(parent, wx.ID_ANY, min=10, max=100000,
                                                 initial=self._DEFAULT_START_FREQUENCY)
        self._add_settings_control('start_frequency', int, self._spin_start_frequency, self._DEFAULT_START_FREQUENCY)

        lbl_stop_frequency = wx.StaticText(parent, wx.ID_ANY, 'Stop frequency [Hz]:')
        self._spin_stop_frequency = wx.SpinCtrl(parent, wx.ID_ANY, min=10, max=100000,
                                                initial=self._DEFAULT_STOP_FREQUENCY)
        self._add_settings_control('stop_frequency', int, self._spin_stop_frequency, self._DEFAULT_STOP_FREQUENCY)

        lbl_log_scale = wx.StaticText(parent, wx.ID_ANY, 'Logarithmic scale:')
        self._chk_log_scale = wx.CheckBox(parent, wx.ID_ANY)
        self._add_settings_control('logarithmic_scale', bool, self._chk_log_scale, False)

        grid = wx.GridBagSizer(self._CONTROLS_SPACING, self._CONTROLS_SPACING)
        grid.Add(lbl_conversion, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._txt_conversion, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_max_amplitude, (1, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._cmb_max_amplitude, (1, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_min_amplitude, (2, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._cmb_min_amplitude, (2, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_start_frequency, (3, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._spin_start_frequency, (3, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_stop_frequency, (4, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._spin_stop_frequency, (4, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_log_scale, (5, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._chk_log_scale, (5, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Spectrum analyzer: '), wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._CONTROLS_SPACING)

        return box

    def _delete_data_at_index(self, data, index):
        indexes = numpy.arange(int(index - 0.5 * self._FREQ_WINDOW),
                               int(index + 1 + 0.5 * self._FREQ_WINDOW))
        too_low = numpy.where(indexes < 0)[0]
        indexes = numpy.delete(indexes, too_low)
        too_high = numpy.where(indexes >= len(data))[0]
        indexes = numpy.delete(indexes, too_high)
        return numpy.delete(data, indexes)

    def _find_max_near_frequency(self, data, frequencies, freq):
        matches = numpy.where(frequencies >= freq)
        index = matches[0][0]
        amplitude = numpy.max(data[int(index - 0.5 * self._FREQ_WINDOW):
                                   int(index + 1 + 0.5 * self._FREQ_WINDOW)])
        index = numpy.where(data == amplitude)[0][0]
        return index

    def _analyze_fft_data(self, fft_data, fft_freq):
        base_amplitude = numpy.max(fft_data)
        base_index = numpy.where(fft_data == base_amplitude)[0][0]
        base_frequency = list(fft_freq)[base_index]

        # FFT without the base frequency
        fft_data = self._delete_data_at_index(fft_data, base_index)
        fft_freq = self._delete_data_at_index(fft_freq, base_index)

        # THD + noise
        thd_noise = 100 * math.sqrt(numpy.sum(numpy.square(fft_data))) / base_amplitude

        harmonics = []
        frequency = base_frequency * 2
        while frequency <= fft_freq[-1]:
            index = self._find_max_near_frequency(fft_data, fft_freq, frequency)
            harmonics.append(fft_data[index])
            fft_data = self._delete_data_at_index(fft_data, index)
            fft_freq = self._delete_data_at_index(fft_freq, index)
            frequency += base_frequency

        # Check the last one
        index = self._find_max_near_frequency(fft_data, fft_freq, fft_freq[-1])
        harmonics.append(fft_data[index])
        fft_data = self._delete_data_at_index(fft_data, index)
        fft_freq = self._delete_data_at_index(fft_freq, index)

        # THD
        thd = 100 * math.sqrt(numpy.sum(numpy.square(numpy.array(harmonics)))) / base_amplitude

        # SNR
        snr = 20 * numpy.log10(base_amplitude / numpy.sqrt(numpy.mean(fft_data ** 2)))

        result = {
            'peak': 20 * numpy.log10(base_amplitude),
            'frequency_peak': base_frequency,
            'thd_noise': thd_noise,
            'thd': thd,
            'snr': snr
        }

        return result


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementSpectrumAnalyzer)
