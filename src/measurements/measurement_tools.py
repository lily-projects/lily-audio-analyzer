"""
Various measurement tools
"""

import numpy
import scipy


class MeasurementTools(object):

    RISING = 'rising'
    FALLING = 'falling'

    _EDGE_SEARCH_VALUES = {
        RISING: (1, 0),
        FALLING: (0, 1)
    }
    _COMP_WINDOW = 0.001

    @staticmethod
    def calculate_peak_peak(samples):
        return max(samples) - min(samples)

    @staticmethod
    def calculate_mean(samples):
        return numpy.mean(samples)

    @staticmethod
    def calculate_rms(samples):
        return numpy.sqrt(numpy.mean(samples ** 2))

    @staticmethod
    def generate_comparator_output(samples, threshold=0.0):
        if abs(threshold) < MeasurementTools._COMP_WINDOW:
            threshold = 0
        threshold_high = threshold + (MeasurementTools._COMP_WINDOW / 2)
        threshold_low = threshold_high - MeasurementTools._COMP_WINDOW
        comp_output = []
        value = 0
        for sample in samples:
            if sample > threshold_high:
                value = 1
            elif sample < threshold_low:
                value = 0
            comp_output.append(value)

        return comp_output

    @staticmethod
    def get_edges(samples, edge=RISING, threshold=0.0):
        comp_output = MeasurementTools.generate_comparator_output(samples, threshold)
        rising_edges = []
        start = 1
        while start < len(comp_output):
            try:
                index = comp_output.index(MeasurementTools._EDGE_SEARCH_VALUES[edge][0], start)
                if comp_output[index - 1] == MeasurementTools._EDGE_SEARCH_VALUES[edge][1]:
                    rising_edges.append(index - 1)
            except ValueError:
                break
            start = index + 1

        return rising_edges

    @staticmethod
    def calculate_frequency(samples, t_values):
        frequency = 0
        t_rising = MeasurementTools.get_edges(samples)
        periods = list(map(lambda i: t_values[t_rising[i]] - t_values[t_rising[i - 1]], range(1, len(t_rising))))
        if len(periods) > 0:
            frequency = len(periods) / sum(periods)

        return frequency

    @staticmethod
    def calculate_fft(samples, sample_rate, db_ref=0.0):
        n_samples = len(samples)
        window = scipy.signal.windows.hann(n_samples)
        frequencies = scipy.fft.rfftfreq(n_samples, 1 / sample_rate)
        amplitudes = (3.27 / n_samples * abs(scipy.fft.rfft(samples * window)))
        if db_ref > 0:
            amplitudes = 20 * numpy.log10(amplitudes / db_ref)
        return frequencies, amplitudes


if __name__ == '__main__':

    from src.unit_test.view_simple_plot import view_simple_plot

    t_frequency = 1000
    t_sample_rate = 48000
    t_n_samples = 250
    t_noise_level_db = -50

    # Generate sine wave
    x_values = numpy.arange(t_n_samples) / t_sample_rate
    y_values = numpy.sin(2 * numpy.pi * t_frequency * x_values)

    # Add some noise for more realistic measurement
    noise_level = 10 ** (t_noise_level_db / 20)
    y_values += noise_level * (numpy.random.rand(t_n_samples) - 0.5)

    _comp_output_1 = MeasurementTools.generate_comparator_output(y_values)
    _comp_output_2 = MeasurementTools.generate_comparator_output(y_values, 0.5)
    _rising_edges = MeasurementTools.get_edges(y_values)
    _falling_edges = MeasurementTools.get_edges(y_values, MeasurementTools.FALLING)
    _t_rising_edges = list(map(lambda x: x_values[x], _rising_edges))
    _t_falling_edges = list(map(lambda x: x_values[x], _falling_edges))

    print('Peak                  : {:.3f}'.format(MeasurementTools.calculate_peak_peak(y_values)))
    print('Mean                  : {:.3f}'.format(MeasurementTools.calculate_mean(y_values)))
    print('RMS                   : {:.3f}'.format(MeasurementTools.calculate_rms(y_values)))
    print('Frequency             : {:.3f}'.format(MeasurementTools.calculate_frequency(y_values, x_values)))
    print('Rising edges (sample) : {}'.format(_rising_edges))
    print('Rising edges (time)   : {}'.format(_t_rising_edges))
    print('Falling edges (sample): {}'.format(_falling_edges))
    print('Falling edges (time)  : {}'.format(_t_falling_edges))

    view_simple_plot(x_values, [y_values, _comp_output_1, _comp_output_2], -1.1, 1.1)

    x_values, y_values = MeasurementTools.calculate_fft(y_values, t_sample_rate, 0.707)

    view_simple_plot(x_values, [y_values], -90, 10)
