"""
Base class for the measurement.

Usage:

class MyMeasurement(MeasurementBase):

    # Your constructor
    def __init__():
        def __init__(self, setting_change_callback):
        name = 'My measurement'
        # See GeneratorControls and MeasurementControls objects for possible options
        generator_options = [
            GeneratorControls.SHOW_...,
            GeneratorControls.SHOW_...
        ]
        measurement_options = [
            MeasurementControls.SHOW_...,
            MeasurementControls.SHOW_...
        ]
        # Call the base class constructor
        super().__init__(name, generator_options, measurement_options, setting_change_callback)
        # Set your default axis legends:
        self.set_plot_data_legends('Frequency [Hz]', 'Amplitude [dB]')


    # Your process data
    def process_data(self, data)

        # processing samples
        x_data = ...                        # generate some x data
        y_data = ...                        # generate some y data
        y_min = ...                         # set the min y scale
        y_max = ...                         # set the max y scale
        optional_log_x_scale = True/False   # optionally set x scale to logarithmic (default False)

        # Update the plot data with the processed samples
        self.update_plot_data(x_data, y_data, y_min, y_max, optional_log_x_scale)

        # Add properties if needed:
        self.add_plot_property('general', 'points', len(x_data))

"""

import numpy
import wx

from src.measurements.measurement_tools import MeasurementTools
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls
from src.models.device_simulator import DeviceSimulator
from src.models.device import Device
from src.models.devices import get_device_from_name
from src.models.generator import Generator
from src.models.grabber import Grabber
from src.models.plot_data import PlotData
from src.models.string_formatter import format_value


class MeasurementBase(object):

    _CONTROLS_SPACING = 5
    _CONTROL_CHANGE_EVENT = {
        wx.CheckBox: wx.EVT_CHECKBOX,
        wx.ComboBox: wx.EVT_COMBOBOX,
        wx.SpinCtrl: wx.EVT_SPINCTRL,
        wx.TextCtrl: wx.EVT_TEXT
    }
    _PLOT_UPDATE_INTERVAL = 300

    def __init__(self, name, generator_options, measurement_options, setting_change_callback):
        self._name = name
        self._generator_options = generator_options
        self._measurement_options = measurement_options
        self._setting_change_callback = setting_change_callback
        self._settings_controls = []
        self._generator = Generator()
        self._grabber = Grabber()
        self._output_device_name = ''
        self._input_device_name = ''
        self._plot_data = PlotData()
        self.set_plot_data_legends('Time [s]', 'Amplitude [V]')
        self._tools = MeasurementTools

    ###################################
    # Private - do not override these #
    ###################################

    def _add_settings_control(self, setting_name, setting_type, setting_control, default_value):
        self._settings_controls.append({
            'name': setting_name,
            'type': setting_type,
            'control': setting_control,
            'default': default_value
        })
        setting_control.Bind(self._CONTROL_CHANGE_EVENT[type(setting_control)],
                             self._on_setting_change)

    def _create_controls_panel(self, parent):
        panel = wx.Panel(parent, wx.ID_ANY)
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(GeneratorControls(panel, self._generator_options, self._add_settings_control),
                0, wx.EXPAND | wx.ALL, self._CONTROLS_SPACING)
        box.Add(MeasurementControls(panel, self._measurement_options, self._add_settings_control),
                0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.BOTTOM, self._CONTROLS_SPACING)
        panel.SetSizer(box)
        return panel

    def _start_generator(self):
        wave_form = self._generator.WAVE_SINE
        frequency = self._generator.DEFAULT_FREQUENCY
        amplitude = self._generator.DEFAULT_AMPLITUDE * 100
        self._generator.set_wave_form(self.get_value_from_control('wave_form', wave_form))
        self._generator.set_frequency(self.get_value_from_control('frequency', frequency))
        self._generator.set_amplitude(float(self.get_value_from_control('amplitude', amplitude)) / 100)
        self._generator.start()

    # Debug function for showing sample data #
    @staticmethod
    def _show_data(data, max_samples=10):
        for i, channel_data in enumerate(data):
            print('CH{:2}'.format(i + 1),
                  '[{:4}]'.format(len(channel_data)),
                  ", ".join(map(lambda x: '{: .8f}'.format(x), channel_data[:max_samples])))

    ##########################################
    # Event handlers - do not override these #
    ##########################################

    def _on_setting_change(self, event):
        self._setting_change_callback()
        control = event.GetEventObject()
        matches = list(filter(lambda x: x['control'] == control, self._settings_controls))
        if len(matches) == 1:
            settings_control = matches[0]
            if settings_control['name'] == 'wave_form':
                self._generator.set_wave_form(settings_control['control'].GetValue())
            elif settings_control['name'] == 'frequency':
                self._generator.set_frequency(int(settings_control['control'].GetValue()))
            elif settings_control['name'] == 'amplitude':
                self._generator.set_amplitude(float(settings_control['control'].GetValue()) / 100)
            elif settings_control['name'] == 'logarithmic_scale':
                self._plot_data.set_x_log_scale(settings_control['control'].GetValue())
            elif settings_control['name'] == 'output_enable':
                if settings_control['control'].GetValue():
                    self._start_generator()
                else:
                    self._generator.stop()

        self.process_changed_settings()
        event.Skip()

    ###################################
    # Override these in the sub class #
    ###################################

    def get_plot_update_interval(self):
        return self._PLOT_UPDATE_INTERVAL

    def process_changed_settings(self):
        sample_rate = self.get_input_sample_rate()
        if sample_rate > 0:
            frequency = self.get_value_from_control('frequency', self._generator.DEFAULT_FREQUENCY)
            # Get about two periods of the data
            n_samples = int(2.2 * sample_rate / frequency)
            self.set_max_samples(n_samples)

    def process_data(self, data):
        max_amplitude = self.get_value_from_control('max_amplitude', MeasurementControls.DEFAULT_MAX_AMPLITUDE)
        sample_rate = self._grabber.get_device().get_default_sample_rate()

        x_data = numpy.arange(len(data[0])) / sample_rate

        self.update_plot_data(x_data, data, -max_amplitude, max_amplitude)
        self.update_plot_property('general', 'Time', format_value(max(x_data), 's'))

        for i, channel_data in enumerate(data):
            self.update_plot_property('channel {}'.format(i + 1), 'Peak-peak',
                                      format_value(self.get_tools().calculate_peak_peak(channel_data), 'V'))

    ##################################
    # Public - do not override these #
    ##################################

    def get_name(self):
        return self._name

    def get_controls_panel(self, parent):
        return self._create_controls_panel(parent)

    def is_running(self):
        return self._generator.is_running() or self._grabber.is_running()

    def get_settings(self):
        settings = {}
        for setting in self._settings_controls:
            try:
                settings[setting['name']] = setting['type'](setting['control'].GetValue())
            except ValueError:
                settings[setting['name']] = setting['default']
        return settings

    def get_value_from_control(self, control_name, default_value):
        matches = list(filter(lambda x: x['name'] == control_name, self._settings_controls))
        if len(matches) > 0:
            try:
                return matches[0]['type'](matches[0]['control'].GetValue())
            except ValueError:
                pass
        return default_value

    def apply_settings(self, settings):
        for setting in self._settings_controls:
            if setting['name'] in settings.keys():
                setting['control'].SetEvtHandlerEnabled(False)
                value = str(settings[setting['name']])
                try:
                    if setting['type'] is bool:
                        value = value.lower() == 'true'
                    setting['control'].SetValue(value)
                except TypeError:
                    setting['control'].SetValue(setting['type'](value))
                setting['control'].SetEvtHandlerEnabled(True)

    def set_output_device_name(self, device_name):
        self._output_device_name = device_name

    def set_input_device_name(self, device_name):
        self._input_device_name = device_name

    def get_input_sample_rate(self):
        sample_rate = 0
        input_device = self._grabber.get_device()
        if input_device is not None:
            sample_rate = input_device.get_default_sample_rate()

        return sample_rate

    def set_max_samples(self, max_samples):
        self._grabber.set_max_samples(max_samples)

    def run(self):
        if not self.is_running():
            # Get OS devices
            output_device = get_device_from_name(self._output_device_name, Device.DEVICE_TYPE_OUTPUT)
            input_device = get_device_from_name(self._input_device_name, Device.DEVICE_TYPE_INPUT)
            if output_device is None and input_device is None:
                # Try simulator
                output_device = DeviceSimulator.get_device_from_name(self._output_device_name)
                input_device = DeviceSimulator.get_device_from_name(self._input_device_name)

            if output_device is not None and input_device is not None:
                self._generator.set_device(output_device)
                self._grabber.set_device(input_device)
                self.process_changed_settings()
                self._grabber.start()
                if self.get_value_from_control('output_enable', False):
                    self._start_generator()

    def stop(self):
        if self.is_running():
            self._generator.stop()
            self._grabber.stop()

    def set_plot_data_legends(self, x_legend, y_legend):
        self._plot_data.set_x_legend(x_legend)
        self._plot_data.set_y_legend(y_legend)

    def update_plot_data(self, x_data, y_data, y_min, y_max, log_scale=None):
        self._plot_data.set_x_data(x_data)
        self._plot_data.set_y_data(y_data)
        self._plot_data.set_y_min(y_min)
        self._plot_data.set_y_max(y_max)
        if log_scale is not None:
            self._plot_data.set_x_log_scale(log_scale)

    def update_plot_property(self, group, label, value):
        self._plot_data.update_plot_property(group, label, value)

    def get_plot_data(self):
        data = self._grabber.get_data()
        if data is not None:
            conversion_factor = self.get_value_from_control('conversion_factor',
                                                            MeasurementControls.DEFAULT_CONVERSION_FACTOR)
            data *= conversion_factor
        self.process_data(data)
        return self._plot_data

    def get_tools(self):
        return self._tools


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement
    from src.unit_test.measurement_test_class import MeasurementBaseTest

    test_measurement(MeasurementBaseTest)
