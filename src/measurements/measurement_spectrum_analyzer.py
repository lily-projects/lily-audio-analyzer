"""
Measurement spectrum analyzer.
Shows the spectrum of the input signal using FFT analysis.
"""

from src.measurements.measurement_base import MeasurementBase
from src.models.generator_controls import GeneratorControls
from src.models.measurement_controls import MeasurementControls


class MeasurementSpectrumAnalyzer(MeasurementBase):

    def __init__(self, setting_change_callback):
        name = 'Spectrum analyzer'
        generator_options = GeneratorControls.SHOW_ALL
        measurement_options = [
            MeasurementControls.SHOW_AMPLITUDE_DB,
            MeasurementControls.SHOW_START_STOP_FREQUENCY,
            MeasurementControls.SHOW_LOG_SCALE
        ]
        super().__init__(name, generator_options, measurement_options, setting_change_callback)


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement

    test_measurement(MeasurementSpectrumAnalyzer)
