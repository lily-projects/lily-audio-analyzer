"""
Main controller.
"""
import os.path

import wx

from src.measurements.measurements_list import get_measurement_by_name
from src.measurements.measurements_list import get_measurement_names
from src.models.application_settings import ApplicationSettings
from src.models.configuration import Configuration
from src.models.devices import get_list_of_input_device_names
from src.models.devices import get_list_of_output_device_names
from src.views.view_dialogs import show_confirm_dialog
from src.views.view_dialogs import show_message
from src.views.view_dialogs import show_open_file
from src.views.view_edit_measurement import ViewEditMeasurement
from src.views.view_logger import ViewLogger
from src.views.view_main import ViewMain
from src.views.view_settings import ViewSettings


class ControllerMain(object):

    def __init__(self, view_title, logger):
        self._logger = logger
        self._logger.debug('Initializing main controller')

        self._logger.debug('Loading application settings')
        self._application_settings = ApplicationSettings(self._logger)

        self._configuration = Configuration()
        filename = self._application_settings.get_reload_configuration_filename()
        if self._application_settings.get_reload_configuration_enabled() and os.path.isfile(filename):
            self._logger.debug('Loading configuration: {}'.format(filename))
            self._configuration.load_from_file(filename)
        else:
            self._logger.debug('Loading empty configuration')

        self._measurement = None

        self._view_logger = ViewLogger(view_title)
        self._view_logger.Bind(wx.EVT_CLOSE, self._on_close_log)
        value = self._application_settings.get_log_window_position()
        if -1 not in value:
            self._view_logger.SetPosition(value)
        value = self._application_settings.get_log_window_size()
        if -1 not in value:
            self._view_logger.SetSize(value)
        self._view_logger.Maximize(self._application_settings.get_log_window_maximized())

        if self._application_settings.get_log_window_auto_open():
            self._view_logger.show()

        self._view_main = ViewMain(view_title)
        self._view_main.Bind(wx.EVT_TOOL, self._on_open_configuration,
                             id=self._view_main.ID_TOOL_OPEN_CONFIGURATION)
        self._view_main.Bind(wx.EVT_TOOL, self._on_save_configuration,
                             id=self._view_main.ID_TOOL_SAVE_CONFIGURATION)
        self._view_main.Bind(wx.EVT_TOOL, self._on_edit_settings,
                             id=self._view_main.ID_TOOL_SETTINGS)
        self._view_main.Bind(wx.EVT_TOOL, self._on_show_log,
                             id=self._view_main.ID_TOOL_LOG)
        self._view_main.Bind(wx.EVT_LIST_ITEM_SELECTED, self._on_measurement_select,
                             id=self._view_main.ID_MEASUREMENT_LIST)
        self._view_main.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self._on_measurement_double_click,
                             id=self._view_main.ID_MEASUREMENT_LIST)
        self._view_main.Bind(wx.EVT_BUTTON, self._on_add_measurement_button,
                             id=self._view_main.ID_BUTTON_ADD_MEASUREMENT)
        self._view_main.Bind(wx.EVT_BUTTON, self._on_delete_measurement_button,
                             id=self._view_main.ID_BUTTON_DELETE_MEASUREMENT)
        self._view_main.Bind(wx.EVT_BUTTON, self._on_run_button,
                             id=self._view_main.ID_BUTTON_RUN)
        self._view_main.Bind(wx.EVT_CLOSE, self._on_main_window_close)

        value = self._application_settings.get_main_window_position()
        if -1 not in value:
            self._view_main.SetPosition(value)
        value = self._application_settings.get_main_window_size()
        if -1 not in value:
            self._view_main.SetSize(value)
        self._view_main.Maximize(self._application_settings.get_main_window_maximized())

        self._view_main.update_configuration(self._configuration)
        self._populate_device_lists()

        self._update_timer = wx.Timer(self._view_main, wx.ID_ANY)
        self._view_main.Bind(wx.EVT_TIMER, self._on_update_timer, self._update_timer)

    ###########
    # Private #
    ###########

    def _populate_device_lists(self):
        self._logger.debug('Generating list of input devices')
        self._view_main.set_input_devices_names(
            get_list_of_input_device_names(self._application_settings.get_add_simulator_devices())
        )
        self._logger.debug('Generating list of output devices')
        self._view_main.set_output_devices_names(
            get_list_of_output_device_names(self._application_settings.get_add_simulator_devices())
        )
        self._view_main.set_active_input_device_name(self._application_settings.get_input_device_name())
        self._view_main.set_active_output_device_name(self._application_settings.get_output_device_name())

    def _set_update_timer_interval(self):
        if self._measurement is not None:
            self._update_timer.Stop()
            self._update_timer.Start(self._measurement.get_plot_update_interval())

    def _stop_measurement(self):
        if self._measurement is not None and self._measurement.is_running():
            self._measurement.stop()

    ##################
    # Event handlers #
    ##################

    def _on_open_configuration(self, event):
        filename = show_open_file(self._view_main, 'Open configuration file',
                                  'Configuration files (*.json)|*.json')
        if filename is not None:
            try:
                self._stop_measurement()
                self._logger.debug('Load configuration from {name}'.format(name=filename))
                self._configuration.load_from_file(filename)
                self._view_main.update_configuration(self._configuration)
            except Exception as e:
                show_message(self._view_main, 'Open configuration',
                             'Could not open the file {filename}\n{error}'.
                             format(filename=filename, error=e), wx.ICON_EXCLAMATION)

        event.Skip()

    def _on_save_configuration(self, event):
        filename = show_open_file(self._view_main, 'Save configuration',
                                  'Configuration files (*.json)|*.json')
        if filename is not None:
            try:
                self._stop_measurement()
                self._logger.debug('Save configuration to {name}'.format(name=filename))
                self._configuration.save_to_file(filename)
                self._view_main.update_configuration(self._configuration, True)
            except Exception as e:
                show_message(self._view_main, 'Save configuration',
                             'Could not save the file {filename}\n{error}'.
                             format(filename=filename, error=e), wx.ICON_EXCLAMATION)
        event.Skip()

    def _on_edit_settings(self, event):
        settings = {
            ViewSettings.KEY_RELOAD_CONFIG: self._application_settings.get_reload_configuration_enabled(),
            ViewSettings.KEY_OPEN_LOG: self._application_settings.get_log_window_auto_open(),
            ViewSettings.KEY_ADD_SIMULATOR: self._application_settings.get_add_simulator_devices()
        }

        dlg = ViewSettings(self._view_main, settings)
        if dlg.ShowModal() == wx.ID_OK:
            settings = dlg.get_settings()
            self._application_settings.store_reload_configuration_enabled(settings[ViewSettings.KEY_RELOAD_CONFIG])
            self._application_settings.store_log_window_auto_open(settings[ViewSettings.KEY_OPEN_LOG])
            self._application_settings.store_add_simulator_devices(settings[ViewSettings.KEY_ADD_SIMULATOR])
            self._populate_device_lists()

        dlg.Destroy()
        event.Skip()

    def _on_show_log(self, event):
        self._view_logger.show()
        self._view_logger.Raise()
        event.Skip()

    def _on_close_log(self, _event):
        self._view_logger.Hide()

    def _on_measurement_select(self, event):
        measurement_name = event.GetText()
        self._stop_measurement()

        self._logger.debug('Select measurement {name}'.format(name=measurement_name))
        settings = self._configuration.get_measurement_settings(measurement_name)
        measurement = get_measurement_by_name(settings.get('type', ''))
        if measurement is not None:
            self._logger.debug('Loading measurement')
            self._measurement = measurement(self._on_setting_change)
            self._logger.debug('Show measurement panel')
            self._view_main.set_measurement_panel(self._measurement.get_controls_panel(
                self._view_main.get_parent_for_measurement_panel()))
            self._logger.debug('Apply measurement settings')
            self._measurement.apply_settings(settings)

        event.Skip()

    def _on_measurement_double_click(self, event):
        measurement_name = event.GetText()
        self._stop_measurement()

        self._logger.debug('Edit measurement {name}'.format(name=measurement_name))
        settings = self._configuration.get_measurement_settings(measurement_name)
        measurement_data = {
            'name': measurement_name,
            'type': settings['type']
        }
        with ViewEditMeasurement(self._view_main, 'Edit measurement',
                                 get_measurement_names(), measurement_data) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                new_measurement_data = dlg.get_measurement_data()
                if measurement_data != new_measurement_data:
                    update_configuration = True
                    if measurement_data['name'] != new_measurement_data['name']:
                        if new_measurement_data['name'] in self._configuration.get_measurement_names():
                            show_message(self._view_main, 'Edit measurement', 'The name already exists.',
                                         wx.ICON_EXCLAMATION)
                            update_configuration = False
                        else:
                            self._logger.debug('Rename measurement {old} to {new}'.format(
                                               old=measurement_data['name'], new=new_measurement_data['name']))
                            self._configuration.update_measurement_name(measurement_data['name'],
                                                                        new_measurement_data['name'])
                    if update_configuration:
                        if measurement_data['type'] != new_measurement_data['type']:
                            self._logger.debug('Measurement changed from {old} to {new}'.format(
                                               old=measurement_data['type'], new=new_measurement_data['type']))
                            self._configuration.update_measurement(new_measurement_data['name'],
                                                                   {'type': new_measurement_data['type']})
                        self._view_main.update_configuration(self._configuration)

        event.Skip()

    def _on_setting_change(self):
        if self._measurement is not None:
            measurement_name = self._view_main.get_selected_measurement_name()
            current_settings = self._configuration.get_measurement_settings(measurement_name)
            measurement_settings = self._measurement.get_settings()
            measurement_settings['type'] = current_settings['type']
            self._configuration.update_measurement(measurement_name, measurement_settings)
            self._view_main.update_configuration(self._configuration, True)

    def _on_add_measurement_button(self, event):
        self._stop_measurement()

        with ViewEditMeasurement(self._view_main, 'Add measurement',
                                 get_measurement_names()) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                measurement_data = dlg.get_measurement_data()
                if measurement_data['name'] in self._configuration.get_measurement_names():
                    show_message(self._view_main, 'Add measurement', 'The name already exists.',
                                 wx.ICON_EXCLAMATION)
                else:
                    self._logger.debug('Add measurement: {data}'.format(data=measurement_data))
                    measurement_name = measurement_data.pop('name')
                    self._configuration.update_measurement(measurement_name, measurement_data)
                    self._view_main.update_configuration(self._configuration)

        event.Skip()

    def _on_delete_measurement_button(self, event):
        self._stop_measurement()

        measurement_name = self._view_main.get_selected_measurement_name()
        if show_confirm_dialog(self._view_main, 'Delete measurement',
                               'Do you want to delete {name}'.format(name=measurement_name),
                               wx.ICON_QUESTION) == wx.ID_YES:
            self._logger.debug('Delete measurement: {name}'.format(name=measurement_name))
            self._configuration.delete_measurement(measurement_name)
            self._view_main.update_configuration(self._configuration)

        event.Skip()

    def _on_run_button(self, event):
        if self._measurement is not None and self._measurement.is_running():
            self._logger.debug('Stop measurement')
            self._measurement.stop()
            self._view_main.set_run_button_label('Run')
        else:
            if self._measurement is None:
                show_message(self._view_main, 'Run measurement', 'Select a measurement.', wx.ICON_INFORMATION)
            else:
                self._logger.debug('Start measurement')
                self._measurement.set_output_device_name(self._view_main.get_selected_output_device_name())
                self._measurement.set_input_device_name(self._view_main.get_selected_input_device_name())
                self._measurement.run()
                self._set_update_timer_interval()
                self._view_main.set_run_button_label('Stop')

        event.Skip()

    def _on_update_timer(self, event):
        if self._measurement is not None:
            if self._measurement.is_running():
                self._view_main.set_run_button_label('Stop')
                self._view_main.update_plot(self._measurement.get_plot_data())
                self._set_update_timer_interval()
            else:
                self._update_timer.Stop()
                self._logger.debug('Measurement has stopped')
                self._view_main.set_run_button_label('Run')

        event.Skip()

    def _on_main_window_close(self, event):
        self._logger.debug('Closing application')
        self._stop_measurement()

        self._application_settings.store_log_window_maximized(self._view_logger.IsMaximized())
        if not self._view_logger.IsMaximized():
            self._application_settings.store_log_window_position(tuple(self._view_logger.GetPosition()))
            self._application_settings.store_log_window_size(tuple(self._view_logger.GetSize()))
        self._view_logger.Destroy()

        self._application_settings.store_input_device_name(self._view_main.get_selected_input_device_name())
        self._application_settings.store_output_device_name(self._view_main.get_selected_output_device_name())

        if self._application_settings.get_reload_configuration_enabled():
            filename = self._configuration.get_filename()
            if os.path.isfile(filename):
                self._application_settings.store_reload_configuration_filename(filename)

        self._application_settings.store_main_window_maximized(self._view_main.IsMaximized())
        if not self._view_main.IsMaximized():
            self._application_settings.store_main_window_position(tuple(self._view_main.GetPosition()))
            self._application_settings.store_main_window_size(tuple(self._view_main.GetSize()))

        event.Skip()

    ##########
    # Public #
    ##########

    def show_view(self):
        self._view_main.Show()


if __name__ == '__main__':

    from src.models.logger import Logger

    test_logger = Logger(True)

    app = wx.App(redirect=False)
    controller = ControllerMain('Test main controller', test_logger)
    controller.show_view()
    app.MainLoop()
