"""
Various dialogs
"""

import wx


def show_open_file(parent, caption, wildcard):
    filename = None
    with wx.FileDialog(parent, caption, wildcard=wildcard, style=wx.FD_OPEN) as dlg:
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()

    return filename


def show_save_file(parent, caption, wildcard):
    filename = None
    with wx.FileDialog(parent, caption, wildcard=wildcard, style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as dlg:
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()

    return filename


def show_message(parent, caption, message, icon):
    with wx.MessageDialog(parent, message, caption, icon) as dlg:
        dlg.ShowModal()


def show_confirm_dialog(parent, caption, message, icon):
    with wx.MessageDialog(parent, message, caption, wx.YES_NO | icon) as dlg:
        return dlg.ShowModal()


if __name__ == '__main__':

    app = wx.App(redirect=False)

    test_filename = show_open_file(None, 'Select a file', 'All files|*.*')
    show_message(None, 'Select a file', 'You selected: {filename}'.format(filename=test_filename),
                 wx.ICON_INFORMATION)
    test_filename = show_save_file(None, 'Save file', 'All files|*.*')
    show_message(None, 'Select a file', 'You selected: {filename}'.format(filename=test_filename),
                 wx.ICON_INFORMATION)

    if show_confirm_dialog(None, 'Question', 'Do you like bacon?', wx.ICON_QUESTION) == wx.ID_YES:
        show_message(None, 'Good', 'Yes! You like bacon.', wx.ICON_INFORMATION)
    else:
        show_message(None, 'Wrong', 'What?! You don\'t like bacon?!', wx.ICON_ERROR)

