"""
Main view for the application.
"""

import wx

from src.models.images import Images
from src.views.view_plot import ViewPlot


class ViewMain(wx.Frame):

    # Toolbar buttons IDs
    ID_TOOL_OPEN_CONFIGURATION = 100
    ID_TOOL_SAVE_CONFIGURATION = 101
    ID_TOOL_OUTPUT_SELECT = 200
    ID_TOOL_INPUT_SELECT = 201
    ID_TOOL_SETTINGS = 202
    ID_TOOL_LOG = 203

    ID_MEASUREMENT_LIST = 300

    ID_BUTTON_ADD_MEASUREMENT = 400
    ID_BUTTON_DELETE_MEASUREMENT = 401

    ID_BUTTON_RUN = 500

    MIN_VIEW_SIZE = (1200, 700)

    _GAP = 5
    _LIST_MEASUREMENT_WIDTH = 240
    _LIST_COL_NAME_SIZE = _LIST_MEASUREMENT_WIDTH - 25

    def __init__(self, view_title):
        self._title = view_title
        super().__init__(None, wx.ID_ANY)
        self.SetTitle(self._title)
        self.SetIcon(wx.Icon(Images.get_bitmap(Images.APP_ICON)))

        self._input_devices_menu = wx.Menu()
        self._input_devices_menu.Append(1, 'Input devices')
        self._output_devices_menu = wx.Menu()
        self._output_devices_menu.Append(1, 'Output devices')

        self._main_panel = wx.Panel(self)

        left_box = wx.BoxSizer(wx.VERTICAL)
        left_box.Add(self._create_measurements_box(self._main_panel), 1, wx.EXPAND)
        left_box.Add(self._create_plot_controls(self._main_panel), 0, wx.EXPAND)

        inner_box = wx.BoxSizer(wx.HORIZONTAL)
        inner_box.Add(left_box, 0, wx.EXPAND | wx.TOP | wx.LEFT | wx.BOTTOM, self._GAP)
        inner_box.Add(self._create_plot_box(self._main_panel), 1, wx.EXPAND | wx.ALL, self._GAP)

        main_box = wx.BoxSizer(wx.VERTICAL)
        main_box.Add(self._create_toolbar(self._main_panel), 0, wx.EXPAND | wx.ALL, self._GAP)
        main_box.Add(self._create_devices_box(self._main_panel), 0, wx.EXPAND | wx.ALL, self._GAP)
        main_box.Add(inner_box, 1, wx.EXPAND)
        self._main_panel.SetSizer(main_box)

        self.SetInitialSize(self.MIN_VIEW_SIZE)

    ###########
    # Private #
    ###########

    def _create_toolbar(self, parent):
        self._toolbar = wx.ToolBar(parent, style=wx.TB_HORIZONTAL | wx.TB_FLAT | wx.TB_NODIVIDER)

        self._toolbar.AddTool(self.ID_TOOL_OPEN_CONFIGURATION, '',
                              Images.get_bitmap(Images.TOOL_OPEN), 'Open configuration')
        self._toolbar.AddTool(self.ID_TOOL_SAVE_CONFIGURATION, '',
                              Images.get_bitmap(Images.TOOL_SAVE),  'Save configuration')
        self._toolbar.AddSeparator()

        self._toolbar.AddTool(self.ID_TOOL_OUTPUT_SELECT, '',
                              Images.get_bitmap(Images.TOOL_SPEAKER), 'Select output device',
                              kind=wx.ITEM_DROPDOWN)
        self._toolbar.SetDropdownMenu(self.ID_TOOL_OUTPUT_SELECT, self._output_devices_menu)
        self._toolbar.AddTool(self.ID_TOOL_INPUT_SELECT, '',
                              Images.get_bitmap(Images.TOOL_MICROPHONE), 'Select input device',
                              kind=wx.ITEM_DROPDOWN)
        self._toolbar.SetDropdownMenu(self.ID_TOOL_INPUT_SELECT, self._input_devices_menu)
        self._toolbar.AddStretchableSpace()

        self._toolbar.AddTool(self.ID_TOOL_SETTINGS, '',
                              Images.get_bitmap(Images.TOOL_SETTINGS), 'Advanced settings')
        self._toolbar.AddSeparator()
        self._toolbar.AddTool(self.ID_TOOL_LOG, '',
                              Images.get_bitmap(Images.TOOL_LOG), 'Show log')

        self._toolbar.Realize()

        return self._toolbar

    def _create_devices_box(self, parent):
        lbl_output_device = wx.StaticText(parent, wx.ID_ANY, 'Output device:')
        lbl_input_device = wx.StaticText(parent, wx.ID_ANY, 'Input device:')
        self._lbl_output_device = wx.StaticText(parent, wx.ID_ANY)
        self._lbl_input_device = wx.StaticText(parent, wx.ID_ANY)

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(lbl_output_device, (0, 0), wx.DefaultSpan)
        grid.Add(self._lbl_output_device, (0, 1), wx.DefaultSpan)
        grid.Add(lbl_input_device, (1, 0), wx.DefaultSpan)
        grid.Add(self._lbl_input_device, (1, 1), wx.DefaultSpan)
        grid.AddGrowableCol(1)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._GAP)

        return box

    def _create_measurements_box(self, parent):
        self._lst_measurements = wx.ListCtrl(parent, self.ID_MEASUREMENT_LIST,
                                             style=wx.LC_REPORT | wx.LC_SINGLE_SEL,
                                             size=(self._LIST_MEASUREMENT_WIDTH, -1))
        self._lst_measurements.InsertColumn(0, 'Name', width=self._LIST_COL_NAME_SIZE)

        panel = wx.Panel(parent, wx.ID_ANY)

        btn_add_measurement = wx.Button(parent, self.ID_BUTTON_ADD_MEASUREMENT, 'Add')
        btn_delete_measurement = wx.Button(parent, self.ID_BUTTON_DELETE_MEASUREMENT, 'Delete')

        grid = wx.GridBagSizer(self._GAP, self._GAP)

        grid.Add(btn_add_measurement, (0, 0), wx.DefaultSpan)
        grid.Add(btn_delete_measurement, (0, 1), wx.DefaultSpan)
        self._measurement_box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY,
                                                               ' Measurements: '), wx.VERTICAL)
        self._measurement_box.Add(self._lst_measurements, 1, wx.EXPAND | wx.ALL, self._GAP)
        self._measurement_box.Add(grid, 0, wx.ALL, self._GAP)
        self._measurement_box.Add(panel, 0, wx.EXPAND | wx.ALL, self._GAP)

        return self._measurement_box

    def _create_plot_controls(self, parent):
        self._btn_run = wx.Button(parent, self.ID_BUTTON_RUN, 'Run')

        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(self._btn_run, 0, wx.EXPAND | wx.ALL, self._GAP)

        return box

    def _create_plot_box(self, parent):
        self._plot = ViewPlot(parent)

        box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Graph: '), wx.VERTICAL)
        box.Add(self._plot, 1, wx.EXPAND | wx.ALL, self._GAP)

        return box

    def _update_device_list(self, dropdown_menu, device_names):
        for menu_item in dropdown_menu.GetMenuItems():
            dropdown_menu.DestroyItem(menu_item)

        for device_name in device_names:
            item = dropdown_menu.AppendRadioItem(wx.ID_ANY, device_name)
            self.Bind(wx.EVT_MENU, self._on_device_select, item)

        self._update_devices_in_status_bar()

    @staticmethod
    def _get_selected_device_name(dropdown_menu):
        device_name = 'unknown'
        for menu_item in dropdown_menu.GetMenuItems():
            if menu_item.IsChecked():
                device_name = menu_item.GetItemLabel()
                break

        return device_name

    def _update_devices_in_status_bar(self):
        self._lbl_output_device.SetLabel(self.get_selected_output_device_name())
        self._lbl_input_device.SetLabel(self.get_selected_input_device_name())

    def _set_active_device_name(self, dropdown_menu, device_name):
        for menu_item in dropdown_menu.GetMenuItems():
            if menu_item.GetItemLabel() == device_name:
                menu_item.Check()
                break
        self._update_devices_in_status_bar()

    ##################
    # Event handlers #
    ##################

    def _on_device_select(self, event):
        self._update_devices_in_status_bar()
        event.Skip()

    ##########
    # Public #
    ##########

    def set_output_devices_names(self, device_names):
        self._update_device_list(self._output_devices_menu, device_names)

    def set_input_devices_names(self, device_names):
        self._update_device_list(self._input_devices_menu, device_names)

    def get_selected_output_device_name(self):
        return self._get_selected_device_name(self._output_devices_menu)

    def get_selected_input_device_name(self):
        return self._get_selected_device_name(self._input_devices_menu)

    def set_active_input_device_name(self, device_name):
        self._set_active_device_name(self._input_devices_menu, device_name)

    def set_active_output_device_name(self, device_name):
        self._set_active_device_name(self._output_devices_menu, device_name)

    def update_configuration(self, configuration, title_only=False):
        title = '{title} - {filename}'.format(title=self._title,
                                              filename=configuration.get_filename())
        if configuration.has_changes():
            title += ' *'
        self.SetTitle(title)

        if not title_only:
            self._lst_measurements.DeleteAllItems()
            for measurement_name in configuration.get_measurement_names():
                self._lst_measurements.InsertItem(self._lst_measurements.GetItemCount(),
                                                  measurement_name)
            self._lst_measurements.SetColumnWidth(0, wx.LIST_AUTOSIZE)
            if self._lst_measurements.GetColumnWidth(0) < self._LIST_COL_NAME_SIZE:
                self._lst_measurements.SetColumnWidth(0, self._LIST_COL_NAME_SIZE)

            self.set_measurement_panel(wx.Panel(self._main_panel))

    def get_parent_for_measurement_panel(self):
        return self._main_panel

    def set_measurement_panel(self, measurement_panel):
        current_panel = self._measurement_box.GetChildren()[2].GetWindow()
        self._measurement_box.Replace(current_panel, measurement_panel)
        current_panel.Destroy()
        self._measurement_box.Layout()

    def get_selected_measurement_name(self):
        measurement_name = None
        index = self._lst_measurements.GetFirstSelected()
        if index >= 0:
            measurement_name = self._lst_measurements.GetItemText(index, 0)

        return measurement_name

    def set_run_button_label(self, label):
        self._btn_run.SetLabel(label)

    def update_plot(self, plot_data):
        self._plot.plot(plot_data)


if __name__ == '__main__':

    from src.models.configuration import Configuration
    from src.measurements.measurements_list import get_measurement_by_name


    def _settings_change_callback():
        print('Change settings')


    config = Configuration()
    config.load_from_file('../../artifacts/configurations/signal.json')
    settings = config.get_measurement_settings(config.get_measurement_names()[0])
    measurement_class = get_measurement_by_name(settings['type'])
    measurement = measurement_class(_settings_change_callback)

    app = wx.App(redirect=False)

    view = ViewMain('Test main view')
    view.Show()
    view.CenterOnScreen()

    view.set_output_devices_names(['Output 1', 'Output 2', 'An output device with a very long name'])
    view.set_input_devices_names(['Input 1', 'Input 2', 'An input device with a very long name'])

    view.set_active_input_device_name('Input 2')
    view.set_active_output_device_name('Output 2')

    view.set_active_input_device_name('Not existing input')
    view.set_active_input_device_name('Not existing output')

    view.update_configuration(config)

    view.set_measurement_panel(measurement.get_controls_panel(
        view.get_parent_for_measurement_panel()))

    app.MainLoop()

    print('\nDone')
