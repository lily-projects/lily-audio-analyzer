"""
Plot view for drawing graphs.
"""

import wx.lib.plot
import wx.adv
import wx.propgrid

from src.models.images import Images
from src.models.string_formatter import format_value


class ViewPlot(wx.Panel):

    MAX_CHANNELS = 8

    # Colors taken from the color_table.png
    _CHANNEL_COLORS = [
        # Basic colors: dark blue, green, dark red, dark orange
        '#0000ab', '#008000', '#8b0000', '#ffac00',
        # Extra colors: light blue, light green, light red (light coral), khaki
        '#add8e6', '#90ee90', '#f08080', '#f0e68c'
    ]

    ID_TOOL_CSV = 1
    ID_TOOL_IMAGE = 2
    ID_TOOL_REDRAW = 3
    ID_TOOL_ZOOM = 4

    _ID_CHANNEL_SELECT = 200

    _PROPERTIES_WIDTH = 250
    _GAP = 5

    def __init__(self, parent):
        super().__init__(parent, wx.ID_ANY)
        self._n_channels = 0
        self._plot_data = None
        self._channel_filter = []

        right_box = wx.BoxSizer(wx.VERTICAL)
        right_box.Add(self._create_channel_selector(), 0, wx.EXPAND)
        right_box.Add(self._create_properties_manager(), 1, wx.EXPAND | wx.TOP, self._GAP)

        inner_box = wx.BoxSizer(wx.HORIZONTAL)
        inner_box.Add(self._create_pot_canvas(), 1, wx.EXPAND)
        inner_box.Add(right_box, 0, wx.EXPAND | wx.LEFT, self._GAP)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._create_toolbar(), 0, wx.EXPAND)
        box.Add(inner_box, 1, wx.EXPAND)

        self.SetSizer(box)

    ###########
    # Private #
    ###########

    def _create_toolbar(self):
        tools = [
            (self.ID_TOOL_REDRAW, Images.TOOL_UNDO, 'Redraw graph'),
            (self.ID_TOOL_ZOOM, Images.TOOL_ZOOM, 'Enable/disable zoom'),
            (self.ID_TOOL_IMAGE, Images.TOOL_IMAGE, 'Export graph to an image'),
            (self.ID_TOOL_CSV, Images.TOOL_CSV, 'Export data to CSV')
        ]
        self._toolbar = wx.ToolBar(self, style=wx.TB_HORIZONTAL | wx.TB_FLAT | wx.TB_NODIVIDER)
        for tool in tools:
            if tool[0] > 0:
                self._toolbar.AddTool(tool[0], '', Images.get_bitmap(tool[1]), tool[2])
            else:
                self._toolbar.AddSeparator()

        self._toolbar.Realize()

        for tool_id in map(lambda x: x[0], tools):
            self._toolbar.Bind(wx.EVT_TOOL, self._on_tool_click, id=tool_id)

        return self._toolbar

    def _create_pot_canvas(self):
        panel = wx.Panel(self, wx.ID_ANY)

        self._plot_canvas = wx.lib.plot.PlotCanvas(panel)
        self._plot_canvas.SetFont(wx.Font(10, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
        self._plot_canvas.fontSizeLegend = 10
        self._plot_canvas.enableLegend = True
        self._plot_canvas.enableTicks = (True, True, False, False)
        self._plot_canvas.enableAntiAliasing = True
        self._plot_canvas.enablePointLabel = True
        self._plot_canvas.xSpec = 'auto'
        self._plot_canvas.ySpec = 'auto'
        self._plot_canvas.canvas.Bind(wx.EVT_MOTION, self._on_motion)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._plot_canvas, 1, wx.EXPAND | wx.ALL, self._GAP)

        panel.SetBackgroundColour(self._plot_canvas.GetBackgroundColour())
        panel.SetSizer(box)

        return panel

    def _create_channel_selector(self):
        self._grid_channel_selector = wx.GridBagSizer(self._GAP, self._GAP)
        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(wx.StaticText(self, wx.ID_ANY, 'Select channels:'), 0, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(self._grid_channel_selector, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.BOTTOM, self._GAP)
        return box

    def _create_properties_manager(self):
        self._properties_manager = wx.propgrid.PropertyGrid(self, wx.ID_ANY, size=(self._PROPERTIES_WIDTH, -1))
        return self._properties_manager

    def _has_data(self):
        return (self._plot_data is not None and
                len(self._plot_data.get_x_data()) > 0 and len(self._plot_data.get_y_data()) > 0)

    def _plot(self):
        if self._has_data():
            if self._n_channels != len(self._plot_data.get_y_data()):
                self._add_channel_selectors()
            self._plot_lines()
            self._add_properties()

    def _add_channel_selectors(self):
        del self._channel_filter[:]
        self._n_channels = len(self._plot_data.get_y_data())

        self._grid_channel_selector.Clear(True)
        row = 0
        col = 0
        for i in range(len(self._plot_data.get_y_data())):
            check_box = wx.CheckBox(self, self._ID_CHANNEL_SELECT + i, 'Channel {}'.format(i + 1))
            check_box.Bind(wx.EVT_CHECKBOX, self._on_channel_select)
            self._grid_channel_selector.Add(check_box, (row, col), wx.DefaultSpan)
            col += 1
            if col == 2:
                col = 0
                row += 1

        if not self._grid_channel_selector.IsColGrowable(0):
            self._grid_channel_selector.AddGrowableCol(0)
        if self._grid_channel_selector.GetCols() > 1:
            if not self._grid_channel_selector.IsColGrowable(1):
                self._grid_channel_selector.AddGrowableCol(1)

        self.Layout()

    def _plot_lines(self):
        if len(self._channel_filter) == 0:
            channel_selectors = map(lambda x: x.GetWindow(), self._grid_channel_selector.GetChildren())
            for selector in channel_selectors:
                selector.SetValue(True)

        x_min = min(self._plot_data.get_x_data())
        x_max = max(self._plot_data.get_x_data())
        y_min = y_max = 0
        graph_objects = []
        for i, channel_data in enumerate(self._plot_data.get_y_data()):
            if len(self._channel_filter) > 0 and i not in self._channel_filter:
                continue

            line_settings = {
                'legend': 'Channel %d' % (i + 1),
                'colour': self._CHANNEL_COLORS[i],
                'width': 2
            }
            line_data = list(zip(self._plot_data.get_x_data(), channel_data))
            graph_objects.append(wx.lib.plot.PolySpline(line_data, **line_settings))

            if max(channel_data) > y_max:
                y_max = max(channel_data)
            if min(channel_data) < y_min:
                y_min = min(channel_data)

        self._plot_canvas.logScale = (self._plot_data.get_x_log_scale(), False)

        gc = wx.lib.plot.PlotGraphics(graph_objects, '',
                                      self._plot_data.get_x_legend(), self._plot_data.get_y_legend())
        self._plot_canvas.Draw(gc, xAxis=(x_min, x_max),
                               yAxis=(self._plot_data.get_y_min(), self._plot_data.get_y_max()))

    def _add_properties(self):
        current_x = '-'
        current_y = '-'
        plot_property = self._properties_manager.GetPropertyByName('cursor x')
        if plot_property is not None:
            current_x = plot_property.GetValue()
        plot_property = self._properties_manager.GetPropertyByName('cursor y')
        if plot_property is not None:
            current_y = plot_property.GetValue()

        self._properties_manager.Clear()
        self._properties_manager.Append(wx.propgrid.PropertyCategory('General'))
        self._add_property('general points', 'Points', str(len(self._plot_data.get_x_data())))
        for plot_property in filter(lambda x: x.get_group() == 'general', self._plot_data.get_plot_properties()):
            self._add_property('{} {}'.format(plot_property.get_group(), plot_property.get_label().lower()),
                               plot_property.get_label(), plot_property.get_value())
        self._add_property('cursor x', 'X cursor', current_x)
        self._add_property('cursor y', 'Y cursor', current_y)
        for i, channel_data in enumerate(self._plot_data.get_y_data()):
            label = 'channel {}'.format(i + 1)
            self._properties_manager.Append(wx.propgrid.PropertyCategory(label))
            for plot_property in filter(lambda x: x.get_group() == label, self._plot_data.get_plot_properties()):
                self._add_property('{} {}'.format(plot_property.get_group().lower(), plot_property.get_label().lower()),
                                   plot_property.get_label(), plot_property.get_value())

    def _add_property(self, name, label, value):
        p = wx.propgrid.StringProperty(label=label, name=name, value=str(value))
        p.ChangeFlag(wx.propgrid.PG_PROP_READONLY, True)
        self._properties_manager.Append(p)

    def _update_cursor(self, point_data):
        plot_property = self._properties_manager.GetPropertyByName('cursor x')
        if plot_property is not None:
            plot_property.SetValue(format_value(point_data[0], decimals=5))
        plot_property = self._properties_manager.GetPropertyByName('cursor y')
        if plot_property is not None:
            plot_property.SetValue(format_value(point_data[1], decimals=5))

    def _save_to_csv(self):
        with wx.FileDialog(self, "Save data to CSV file", wildcard="csv files (*.csv)|*.csv",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as dialog:
            if dialog.ShowModal() != wx.ID_CANCEL:
                icon = wx.ICON_INFORMATION
                try:
                    filename = dialog.GetPath()
                    with open(filename, 'w') as fp:
                        header = ['time']
                        for i in range(len(self._plot_data.get_y_data())):
                            header.append('channel %d' % (i + 1))
                        fp.write('%s\n' % ','.join(header))
                        for i, x_value in enumerate(self._plot_data.get_x_data()):
                            data_out = [x_value]
                            for channel_data in self._plot_data.get_y_data():
                                data_out.append(channel_data[i])
                            fp.write('%s\n' % ','.join(map(lambda y_value: str(y_value), data_out)))

                    message = 'Data exported to: %s' % filename
                except Exception as e:
                    icon = wx.ICON_EXCLAMATION
                    message = 'Error saving file: %s\n%s' % (filename, e)

                with wx.MessageDialog(self, message, 'Save to CSV', style=wx.OK | icon) as message:
                    message.ShowModal()

    def _enable_zoom(self, enable):
        self._plot_canvas.enableZoom = enable
        self._plot_canvas.showScrollbars = enable

    ##################
    # Event handlers #
    ##################

    def _on_motion(self, event):
        if self._has_data():
            point = self._plot_canvas.GetXY(event)
            x_range = self._plot_canvas.xCurrentRange
            y_range = self._plot_canvas.yCurrentRange
            if x_range[0] <= point[0] <= x_range[1] and y_range[0] <= point[1] <= y_range[1]:
                self._plot_canvas.SetCursor(wx.Cursor(wx.CURSOR_CROSS))
                self._update_cursor(point)
            else:
                self._plot_canvas.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        event.Skip()

    def _on_tool_click(self, event):
        if self._has_data():
            tool_id = event.GetId()
            if tool_id == self.ID_TOOL_CSV:
                self._save_to_csv()

            elif tool_id == self.ID_TOOL_IMAGE:
                self._plot_canvas.SaveFile()

            elif tool_id == self.ID_TOOL_REDRAW:
                self._enable_zoom(False)
                self._plot()

            elif tool_id == self.ID_TOOL_ZOOM:
                self._enable_zoom(not self._plot_canvas.enableZoom)
                if not self._plot_canvas.enableZoom:
                    self._plot()

        event.Skip()

    def _on_channel_select(self, event):
        del self._channel_filter[:]
        channel_selectors = map(lambda x: x.GetWindow(), self._grid_channel_selector.GetChildren())
        for selector in channel_selectors:
            if selector.GetValue():
                self._channel_filter.append(selector.GetId() - self._ID_CHANNEL_SELECT)

        wx.CallAfter(self._plot)
        event.Skip()

    ##########
    # Public #
    ##########

    def plot(self, plot_data_object):
        if not self._plot_canvas.enableZoom:
            self._plot_data = plot_data_object
            self._plot()


if __name__ == '__main__':

    import numpy
    import random

    from src.models.plot_data import PlotData


    class _ViewTestPlot(wx.Frame):

        _GAP = 5
        _UPDATE_INTERVAL = 200

        def __init__(self):
            super().__init__(None, wx.ID_ANY, 'Test Plot')
            self.SetInitialSize((900, 600))
            self.SetPosition((100, 50))
            panel = wx.Panel(self)
            box = wx.BoxSizer(wx.VERTICAL)
            box.Add(self._create_graph_box(panel), 1, wx.EXPAND | wx.ALL, self._GAP)
            box.Add(self._create_controls_box(panel), 0, wx.EXPAND | wx.ALL, self._GAP)
            panel.SetSizer(box)

            self.Bind(wx.EVT_CLOSE, self._on_close)

            self._update_timer = wx.Timer(self, wx.ID_ANY)
            self.Bind(wx.EVT_TIMER, self._on_update_timer, self._update_timer)

        ###########
        # Private #
        ###########

        def _create_graph_box(self, parent):
            self._plot = ViewPlot(parent)
            box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Graph: '), wx.VERTICAL)
            box.Add(self._plot, 1, wx.EXPAND | wx.ALL, self._GAP)
            return box

        def _create_controls_box(self, parent):
            lbl_channels = wx.StaticText(parent, wx.ID_ANY, 'Number of channels:')
            self._cmb_channels = wx.ComboBox(parent, wx.ID_ANY, style=wx.CB_READONLY)
            self._cmb_channels.SetItems(list(map(lambda x: str(x + 1), range(self._plot.MAX_CHANNELS))))
            self._cmb_channels.SetValue('2')

            btn_draw = wx.Button(parent, wx.ID_ANY, 'Draw')
            btn_draw.Bind(wx.EVT_BUTTON, self._on_button_draw)

            btn_timed_updates = wx.Button(parent, wx.ID_ANY, 'Start timed updates')
            btn_timed_updates.Bind(wx.EVT_BUTTON, self._on_button_timed_updates)

            grid = wx.GridBagSizer(self._GAP, self._GAP)
            grid.Add(lbl_channels, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.Add(self._cmb_channels, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.Add(btn_draw, (2, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            grid.Add(btn_timed_updates, (3, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

            box = wx.StaticBoxSizer(wx.StaticBox(parent, wx.ID_ANY, ' Test: '), wx.VERTICAL)
            box.Add(grid, 1, wx.EXPAND | wx.ALL, self._GAP)

            return box

        @staticmethod
        def _create_plot_data(x_data, y_data):
            plot_data = PlotData()
            plot_data.set_x_data(x_data)
            plot_data.set_y_data(y_data)
            plot_data.set_y_min(-1.1)
            plot_data.set_y_max(1.1)
            plot_data.set_x_legend('Time [s]')
            plot_data.set_y_legend('Amplitude [V]')
            plot_data.update_plot_property('general', 'Time', format_value(max(plot_data.get_x_data()), 's'))

            for i, channel_data in enumerate(plot_data.get_y_data()):
                plot_data.update_plot_property('channel {}'.format(i + 1), 'Peak-peak',
                                               format_value(max(channel_data) - min(channel_data), 'V'))

            return plot_data

        ##################
        # Event handlers #
        ##################

        def _on_button_draw(self, event):
            n_channels = int(self._cmb_channels.GetValue())

            x_data = []
            y_data = []
            for x in numpy.arange(0, 1.001, 0.001):
                x_data.append(x)
                for j in range(n_channels):
                    if len(y_data) < n_channels:
                        y_data.append([])
                    y_data[j].append(numpy.sin(20 * (x - (0.03 * j))))

            self._plot.plot(self._create_plot_data(x_data, y_data))
            event.Skip()

        def _on_button_timed_updates(self, event):
            button = event.GetEventObject()
            if self._update_timer.IsRunning():
                self._update_timer.Stop()
                button.SetLabel('Start timed updates')
            else:
                self._update_timer.Start()
                button.SetLabel('Stop timed updates')

        def _on_update_timer(self, event):
            n_channels = int(self._cmb_channels.GetValue())

            x_data = []
            y_data = []
            for x in numpy.arange(0, 1.001, 0.001):
                x_data.append(x)
                for j in range(n_channels):
                    if len(y_data) < n_channels:
                        y_data.append([])
                    y_data[j].append(random.uniform(-0.5, 0.5))

            self._plot.plot(self._create_plot_data(x_data, y_data))
            event.Skip()

        def _on_close(self, event):
            if self._update_timer.IsRunning():
                self._update_timer.Stop()

            event.Skip()


    app = wx.App(redirect=False)

    view = _ViewTestPlot()
    view.Show()

    app.MainLoop()

    print('\nDone')
