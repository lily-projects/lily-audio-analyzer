"""
Settings dialog.
"""

import wx


class ViewSettings(wx.Dialog):

    KEY_RELOAD_CONFIG = 'reload_config'
    KEY_OPEN_LOG = 'open_log'
    KEY_ADD_SIMULATOR = 'add_simulator'

    _TITLE = 'Advanced settings'
    _GAP = 10
    _WINDOW_SIZE = (300, -1)

    def __init__(self, parent, settings):
        super().__init__(parent, wx.ID_ANY, self._TITLE)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(self._create_settings_controls(), 1, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(self._create_button_controls(), 0, wx.CENTER | wx.ALL, self._GAP)

        for key in settings.keys():
            if key == self.KEY_RELOAD_CONFIG:
                self._chk_reload_config.SetValue(settings[key])
            if key == self.KEY_OPEN_LOG:
                self._chk_open_log.SetValue(settings[key])
            if key == self.KEY_ADD_SIMULATOR:
                self._chk_add_simulator.SetValue(settings[key])

        self.SetSizer(box)
        self.SetInitialSize(self._WINDOW_SIZE)
        self.CenterOnParent()

    ###########
    # Private #
    ###########

    def _create_settings_controls(self):
        lbl_reload_config = wx.StaticText(self, wx.ID_ANY, 'Reload configuration at startup')
        self._chk_reload_config = wx.CheckBox(self, wx.ID_ANY)
        lbl_open_log = wx.StaticText(self, wx.ID_ANY, 'Open log window at startup')
        self._chk_open_log = wx.CheckBox(self, wx.ID_ANY)
        lbl_add_simulator = wx.StaticText(self, wx.ID_ANY, 'Add simulator to device list')
        self._chk_add_simulator = wx.CheckBox(self, wx.ID_ANY)

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(lbl_reload_config, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._chk_reload_config, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_open_log, (1, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._chk_open_log, (1, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_add_simulator, (2, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(self._chk_add_simulator, (2, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        return grid

    def _create_button_controls(self):
        btn_cancel = wx.Button(self, wx.ID_CANCEL, 'Cancel')
        btn_ok = wx.Button(self, wx.ID_OK, 'OK')

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(btn_cancel, (0, 0), wx.DefaultSpan)
        grid.Add(btn_ok, (0, 1), wx.DefaultSpan)

        return grid

    ##########
    # Public #
    ##########

    def get_settings(self):
        return {
            self.KEY_RELOAD_CONFIG: self._chk_reload_config.GetValue(),
            self.KEY_OPEN_LOG: self._chk_open_log.GetValue(),
            self.KEY_ADD_SIMULATOR: self._chk_add_simulator.GetValue()
        }


if __name__ == '__main__':

    test_settings = {}

    app = wx.App(redirect=False)

    dlg = ViewSettings(None, test_settings)
    if dlg.ShowModal() == wx.ID_OK:
        test_settings = dlg.get_settings()
        dlg.Destroy()
        print(test_settings)

        dlg = ViewSettings(None, test_settings)
        if dlg.ShowModal() == wx.ID_OK:
            test_settings = dlg.get_settings()
            print(test_settings)

    dlg.Destroy()

    app.MainLoop()
