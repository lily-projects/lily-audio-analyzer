"""
View for editing the measurement.
"""

import wx

from src.views.view_dialogs import show_message


class ViewEditMeasurement(wx.Dialog):

    _GAP = 10
    _INITIAL_SIZE = (400, -1)

    def __init__(self, parent_view, title, measurement_names, measurement_data=None):
        if measurement_data is None:
            measurement_data = {}
            
        super().__init__(parent_view)
        self.SetTitle(title)

        lbl_name = wx.StaticText(self, wx.ID_ANY, 'Name:')
        self._txt_name = wx.TextCtrl(self, wx.ID_ANY)
        lbl_measurement = wx.StaticText(self, wx.ID_ANY, 'Measurement')
        self._cmb_measurements = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY)
        self._cmb_measurements.SetItems(measurement_names)

        if 'name' in measurement_data.keys():
            self._txt_name.SetValue(measurement_data['name'])
        if 'type' in measurement_data.keys():
            self._cmb_measurements.SetValue(measurement_data['type'])

        btn_ok = wx.Button(self, wx.ID_OK, 'Ok')
        btn_ok.Bind(wx.EVT_BUTTON, self._on_ok_button)
        btn_cancel = wx.Button(self, wx.ID_CANCEL, 'Cancel')

        grid = wx.GridBagSizer(self._GAP, self._GAP)
        grid.Add(lbl_name, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTRE_VERTICAL)
        grid.Add(self._txt_name, (0, 1), wx.DefaultSpan, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL)
        grid.Add(lbl_measurement, (1, 0), wx.DefaultSpan, wx.ALIGN_CENTRE_VERTICAL)
        grid.Add(self._cmb_measurements, (1, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.AddGrowableCol(1)

        buttons_box = wx.BoxSizer(wx.HORIZONTAL)
        buttons_box.Add(btn_ok, 0, wx.ALL, self._GAP)
        buttons_box.Add(btn_cancel, 0, wx.ALL, self._GAP)

        box = wx.BoxSizer(wx.VERTICAL)
        box.Add(grid, 0, wx.EXPAND | wx.ALL, self._GAP)
        box.Add(buttons_box, 0, wx.ALIGN_CENTER | wx.ALL, self._GAP)

        self.SetSizer(box)
        self.SetInitialSize(self._INITIAL_SIZE)
        self.CenterOnParent()

    ##################
    # Event handlers #
    ##################

    def _on_ok_button(self, event):
        if self._txt_name.GetValue().strip() == '':
            show_message(self, self.GetTitle(), 'Enter a name.', wx.ICON_EXCLAMATION)
            return

        if self._cmb_measurements.GetValue() == '':
            show_message(self, self.GetTitle(), 'Select a measurement.', wx.ICON_EXCLAMATION)
            return

        event.Skip()

    ##########
    # Public #
    ##########

    def get_measurement_data(self):
        return {
            'name': self._txt_name.GetValue().strip(),
            'type': self._cmb_measurements.GetValue()
        }


if __name__ == '__main__':

    test_measurement_names = ['ham', 'spam', 'bacon']

    app = wx.App(redirect=False)

    with ViewEditMeasurement(None, 'Add measurement', test_measurement_names) as dlg:
        if dlg.ShowModal() == wx.ID_OK:
            print(dlg.get_measurement_data())

    test_measurement_data = {
        'name': 'And now for something completely different',
        'type': 'bacon'
    }
    with ViewEditMeasurement(None, 'Edit measurement', test_measurement_names,
                             test_measurement_data) as dlg:
        if dlg.ShowModal() == wx.ID_OK:
            print(dlg.get_measurement_data())

    test_measurement_data = {
        'name': 'A not existing measurement',
        'type': 'this does not exist'
    }
    with ViewEditMeasurement(None, 'Edit measurement', test_measurement_names,
                             test_measurement_data) as dlg:
        if dlg.ShowModal() == wx.ID_OK:
            print(dlg.get_measurement_data())
