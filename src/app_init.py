"""
Application init file.
"""

import os


APP_NAME = 'Lily Audio Analyzer'
EXE_NAME = 'LilyAudioAnalyzer'
VERSION = '1.0'
COMPANY = 'LilyTronics'

USER_FOLDER = os.path.join(os.path.expanduser('~'))
