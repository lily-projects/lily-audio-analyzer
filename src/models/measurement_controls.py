"""
Widget with measurement controls.
"""

import wx

from src.models.generator_controls import generate_controls


class MeasurementControls(wx.StaticBoxSizer):

    SHOW_SENSE_RESISTOR = 'show_sense_resistor'
    SHOW_MAX_TIME = 'show_max_time'
    SHOW_AMPLITUDE = 'show_amplitude'
    SHOW_TRIGGER = 'show_trigger'
    SHOW_AMPLITUDE_DB = 'show_amplitude_db'
    SHOW_IMPEDANCE = 'show_impedance'
    SHOW_START_STOP_FREQUENCY = 'show_start_stop_frequency'
    SHOW_LOG_SCALE = 'show_log_scale'

    SHOW_ALL = [
        SHOW_SENSE_RESISTOR,
        SHOW_MAX_TIME,
        SHOW_AMPLITUDE,
        SHOW_TRIGGER,
        SHOW_AMPLITUDE_DB,
        SHOW_IMPEDANCE,
        SHOW_START_STOP_FREQUENCY,
        SHOW_LOG_SCALE
    ]

    DEFAULT_CONVERSION_FACTOR = 1
    DEFAULT_SENSE_RESISTOR = 100
    DEFAULT_MAX_TIME = '5ms'
    DEFAULT_MAX_AMPLITUDE = 1
    DEFAULT_TRIGGER_EDGE = 'rising'
    DEFAULT_TRIGGER_LEVEL = 0
    DEFAULT_MAX_AMPLITUDE_DB = 10
    DEFAULT_MIN_AMPLITUDE_DB = -150
    DEFAULT_MAX_IMPEDANCE = 1000
    DEFAULT_MIN_IMPEDANCE = 0
    DEFAULT_START_FREQUENCY = 10
    DEFAULT_STOP_FREQUENCY = 22000

    _BOX_CAPTION = 'Measurement'
    _CONTROLS_SPACING = 5

    # 44.1kHz sample frequency => about 23us sample time
    # Minimum samples for showing wave form: 20 => 20 * 23 = 460us
    # Time range: 500us to 1s
    # Time range max is not included in the list, set time range max 1 step higher
    _TIME_RANGE_MIN = '500us'
    _TIME_RANGE_MAX = '2s'
    _TIME_UNITS = ['us', 'ms', 's']
    _TIME_VALUES = ['1', '2', '5', '10', '20', '50', '100', '200', '500']
    _DB_RANGE_MAX = 40
    _DB_RANGE_MIN = -160

    def __init__(self, parent, view_options, add_control_callback):
        super().__init__(wx.StaticBox(parent, wx.ID_ANY, ' {}: '.format(self._BOX_CAPTION)), wx.VERTICAL)

        self._row = 0
        self._grid = wx.GridBagSizer(self._CONTROLS_SPACING, self._CONTROLS_SPACING)

        # Always show the conversion factor
        label, control = generate_controls(parent, 'Conversion factor', wx.TextCtrl,
                                           default=str(self.DEFAULT_CONVERSION_FACTOR))
        self._add_to_grid(label, control)
        add_control_callback('conversion_factor', float, control, self.DEFAULT_CONVERSION_FACTOR)

        if self.SHOW_SENSE_RESISTOR in view_options:
            label, control = generate_controls(parent, 'Sense resistor [Ohm]', wx.TextCtrl,
                                               default=str(self.DEFAULT_SENSE_RESISTOR))
            self._add_to_grid(label, control)
            add_control_callback('sense_resistor', float, control, self.DEFAULT_SENSE_RESISTOR)

        if self.SHOW_MAX_TIME in view_options:
            items = []
            add_value = False
            for unit in self._TIME_UNITS:
                for value in self._TIME_VALUES:
                    time_value = '{}{}'.format(value, unit)
                    if time_value == self._TIME_RANGE_MIN:
                        add_value = True
                    if time_value == self._TIME_RANGE_MAX:
                        add_value = False
                    if add_value:
                        items.append(time_value)

            label, control = generate_controls(parent, 'Max time', wx.ComboBox, choices=items,
                                               default=self.DEFAULT_MAX_TIME)
            self._add_to_grid(label, control)
            add_control_callback('max_time', str, control, self.DEFAULT_MAX_TIME)

        if self.SHOW_AMPLITUDE in view_options:
            label, control = generate_controls(parent, 'Max amplitude [V]', wx.TextCtrl,
                                               default=str(self.DEFAULT_MAX_AMPLITUDE))
            self._add_to_grid(label, control)
            add_control_callback('max_amplitude', float, control, self.DEFAULT_MAX_AMPLITUDE)

        if self.SHOW_TRIGGER in view_options:
            label, control = generate_controls(parent, 'Trigger edge', wx.ComboBox, choices=['rising', 'falling'],
                                               default=self.DEFAULT_TRIGGER_EDGE)
            self._add_to_grid(label, control)
            add_control_callback('trigger_edge', str, control, self.DEFAULT_TRIGGER_EDGE)

            label, control = generate_controls(parent, 'Trigger level [V]', wx.TextCtrl,
                                               default=str(self.DEFAULT_TRIGGER_LEVEL))
            self._add_to_grid(label, control)
            add_control_callback('trigger_level', float, control, self.DEFAULT_TRIGGER_LEVEL)

        if self.SHOW_AMPLITUDE_DB in view_options:
            amplitude_db_values = [str(x) for x in range(self._DB_RANGE_MAX, self._DB_RANGE_MIN, -10)]
            label, control = generate_controls(parent, 'Max amplitude [dBV]', wx.ComboBox,
                                               choices=amplitude_db_values[:-1],
                                               default=str(self.DEFAULT_MAX_AMPLITUDE_DB))
            self._add_to_grid(label, control)
            add_control_callback('max_amplitude_db', int, control, self.DEFAULT_MAX_AMPLITUDE_DB)

            label, control = generate_controls(parent, 'Min amplitude [dBV]', wx.ComboBox,
                                               choices=amplitude_db_values[1:],
                                               default=str(self.DEFAULT_MIN_AMPLITUDE_DB))
            self._add_to_grid(label, control)
            add_control_callback('min_amplitude_db', int, control, self.DEFAULT_MIN_AMPLITUDE_DB)

        if self.SHOW_IMPEDANCE in view_options:
            label, control = generate_controls(parent, 'Max impedance', wx.TextCtrl,
                                               default=str(self.DEFAULT_MAX_IMPEDANCE))
            self._add_to_grid(label, control)
            add_control_callback('max_impedance', int, control, self.DEFAULT_MAX_IMPEDANCE)

            label, control = generate_controls(parent, 'Min impedance', wx.TextCtrl,
                                               default=str(self.DEFAULT_MIN_IMPEDANCE))
            self._add_to_grid(label, control)
            add_control_callback('min_impedance', int, control, self.DEFAULT_MIN_IMPEDANCE)

        if self.SHOW_START_STOP_FREQUENCY in view_options:
            label, control = generate_controls(parent, 'Start frequency [Hz]', wx.SpinCtrl,
                                               min=10, max=100000,
                                               default=self.DEFAULT_START_FREQUENCY)
            self._add_to_grid(label, control)
            add_control_callback('start_frequency', int, control, self.DEFAULT_START_FREQUENCY)

            label, control = generate_controls(parent, 'Stop frequency [Hz]', wx.SpinCtrl,
                                               min=10, max=100000,
                                               default=self.DEFAULT_STOP_FREQUENCY)
            self._add_to_grid(label, control)
            add_control_callback('stop_frequency', int, control, self.DEFAULT_STOP_FREQUENCY)

        if self.SHOW_LOG_SCALE in view_options:
            label, control = generate_controls(parent, 'Logarithmic scale', wx.CheckBox, default=False)
            self._add_to_grid(label, control)
            add_control_callback('logarithmic_scale', bool, control, False)

        self.Add(self._grid, 0, wx.EXPAND | wx.ALL, self._CONTROLS_SPACING)

    ###########
    # Private #
    ###########

    def _add_to_grid(self, label, control):
        self._grid.Add(label, (self._row, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        self._grid.Add(control, (self._row, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        self._row += 1


if __name__ == '__main__':

    from src.unit_test.view_test_controls import show_view_test_controls

    show_view_test_controls()
