"""
Output stream object.
Can be a sound device stream or a simulator stream.
"""

import sounddevice

from src.models.device_simulator import DeviceSimulator


class OutputStream(object):

    def __init__(self, device, callback_function):
        device_index = device.get_index()
        if device_index == DeviceSimulator.INDEX:
            self._output_stream = DeviceSimulator.get_output_stream(callback_function)
        else:
            sample_rate = device.get_default_sample_rate()
            self._output_stream = sounddevice.OutputStream(device=device_index,
                                                           samplerate=sample_rate,
                                                           callback=callback_function)

    def start(self): self._output_stream.start()
    def stop(self): self._output_stream.stop()
    def is_active(self): return self._output_stream.active


if __name__ == '__main__':

    import time

    from devices import select_output_device

    def _callback_function(*params):
        print(params)


    test_device = select_output_device(True)
    if test_device is None:
        exit(1)

    output_stream = OutputStream(test_device, _callback_function)
    output_stream.start()
    time.sleep(1)
    print('Is active:', output_stream.is_active())
    output_stream.stop()
    print('Is active:', output_stream.is_active())
