"""
Images model.
"""

import io
import wx

from src.models.image_data import IMAGE_DATA


class Images(object):

    APP_ICON = 'audio-wave-24.png'
    TOOL_CSV = 'csv-24.png'
    TOOL_IMAGE = 'image-2-24.png'
    TOOL_LOG = 'text-file-6-24.png'
    TOOL_MICROPHONE = 'microphone-3-24.png'
    TOOL_OPEN = 'folder-3-24.png'
    TOOL_SAVE = 'save-24.png'
    TOOL_SETTINGS = 'cog-24.png'
    TOOL_SPEAKER = 'speaker-24.png'
    TOOL_UNDO = 'undo-4-24.png'
    TOOL_ZOOM = 'zoom-24.png'

    @classmethod
    def get_bitmap(cls, image_name): return wx.Bitmap(wx.Image(io.BytesIO(IMAGE_DATA[image_name])))


if __name__ == '__main__':

    app = wx.App(redirect=False)
    for key in sorted([x for x in list(Images.__dict__.keys()) if x.startswith('TOOL_')]):
        print(Images.get_bitmap(Images.__dict__[key]))

    print('\nDone')
