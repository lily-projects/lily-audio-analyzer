"""
Simulates an audio device as to use in the application.
It functions as a loopback between input and output.
The loop back can have various functions like a filter, attenuator, amplifier and so on.

The simulator is only available in the test application, in the normal application, there is no simulator available.
"""

import numpy
import queue
import threading
import time

from src.models.device import Device


class DeviceSimulator(object):

    ####################
    # Simulator thread #
    ####################

    class SimulatorThread(object):

        _NOISE_LEVEL = -60   # dB

        def __init__(self, stream_queue, frame_interval):
            self._stream_queue = stream_queue
            self._sample_rate = 0
            self._frame_interval = frame_interval
            self._channels = 1
            self.active = False
            self._stop_event = threading.Event()
            self._thread = None
            self._generator_callback = None
            self._grabber_callback = None
            self._function = self._function_loopback

        ###########
        # Private #
        ###########

        def _simulate(self):
            self.active = True

            while not self._stop_event.is_set():
                frame_size = int(self._frame_interval * self._sample_rate)

                if self._generator_callback is not None:
                    data = numpy.empty([frame_size, self._channels])
                    self._generator_callback(data, frame_size, None, None)
                    self._stream_queue.put(self._function(data))

                if self._grabber_callback is not None:
                    noise_level = 10 ** (self._NOISE_LEVEL / 20)
                    data = noise_level * (numpy.random.rand(frame_size, self._channels) - 0.5)
                    try:
                        data += self._stream_queue.get_nowait()
                    except queue.Empty:
                        pass

                    self._grabber_callback(data, None, None, None)

                time.sleep(self._frame_interval)

            self.active = False
            self._thread = None

        @staticmethod
        def _function_loopback(data):
            return data

        ##########
        # Public #
        ##########

        def set_generator_callback(self, callback):
            self._generator_callback = callback

        def set_grabber_callback(self, callback):
            self._grabber_callback = callback

        def set_channels(self, n_channels):
            self._channels = n_channels

        def set_sample_rate(self, sample_rate):
            self._sample_rate = sample_rate

        def set_function(self, function_name):
            matches = list(filter(lambda x: x.startswith('_function_{name}'.format(name=function_name)), dir(self)))
            if len(matches) == 1:
                self._function = getattr(self, matches[0])

        def start(self):
            if self._thread is not None and self._thread.is_alive():
                self.stop()

            self._thread = threading.Thread(target=self._simulate)
            self._thread.daemon = True
            self._stop_event.clear()
            self._thread.start()

        def stop(self):
            if self._thread.is_alive():
                self._stop_event.set()
                self._thread.join()

    ####################
    # Device Simulator #
    ####################

    INDEX = -17121974

    _INPUT_PREFIX = 'Microphone'
    _OUTPUT_PREFIX = 'Speakers'
    _NAME = 'Lily Audio Device Simulator'
    _N_CHANNELS = [1, 2, 4]
    _SAMPLE_RATES = [48000, 96000, 192000]
    _FRAME_INTERVAL = 0.025
    _MAX_LATENCY = 0.01
    _FUNCTIONS = ['loopback']
    _NAME_FORMAT = '{prefix} ({name}), {channels} channels, {sample_rate}Hz, {function_name}'

    _STREAM_QUEUE = queue.Queue()
    _INPUT_STREAM = SimulatorThread(_STREAM_QUEUE, _FRAME_INTERVAL)
    _OUTPUT_STREAM = SimulatorThread(_STREAM_QUEUE, _FRAME_INTERVAL)

    @classmethod
    def _generate_device_names(cls, device_type):
        names = []

        if device_type == Device.DEVICE_TYPE_INPUT:
            prefix = cls._INPUT_PREFIX
        elif device_type == Device.DEVICE_TYPE_OUTPUT:
            prefix = cls._OUTPUT_PREFIX
        else:
            return names

        for sample_rate in cls._SAMPLE_RATES:
            for channel in cls._N_CHANNELS:
                for function_name in cls._FUNCTIONS:
                    names.append(cls._NAME_FORMAT.format(
                        prefix=prefix,
                        name=cls._NAME,
                        channels=channel,
                        sample_rate=sample_rate,
                        function_name=function_name
                    ))
        return names

    ##########
    # Public #
    ##########

    @classmethod
    def get_device_names(cls, device_type):
        return cls._generate_device_names(device_type)

    @classmethod
    def get_device_from_name(cls, device_name):
        if cls._NAME not in device_name:
            return None

        parts = device_name.split(',')
        name = parts[0].strip()
        n_channels = int(parts[1].strip().split(' ')[0])
        sample_rate = int(parts[2].strip().replace('Hz', ''))
        function = parts[3].strip()
        device_data = {
            'index': cls.INDEX,
            'name': name,
            'hostapi': 0,
            'max_input_channels': 0,
            'max_output_channels': 0,
            'default_samplerate': sample_rate,
            'default_high_output_latency': cls._MAX_LATENCY,
            'default_high_input_latency': cls._MAX_LATENCY,
            'simulator_function': function
        }
        if device_name.startswith(cls._INPUT_PREFIX):
            device_data['max_input_channels'] = n_channels
            cls._INPUT_STREAM.set_channels(n_channels)
            cls._INPUT_STREAM.set_sample_rate(sample_rate)
        elif device_name.startswith(cls._OUTPUT_PREFIX):
            device_data['max_output_channels'] = n_channels
            cls._OUTPUT_STREAM.set_channels(n_channels)
            cls._OUTPUT_STREAM.set_sample_rate(sample_rate)
            cls._OUTPUT_STREAM.set_function(device_data['simulator_function'])

        return Device(device_data)

    @classmethod
    def get_input_stream(cls, callback):
        cls._INPUT_STREAM.set_grabber_callback(callback)
        return cls._INPUT_STREAM

    @classmethod
    def get_output_stream(cls, callback):
        cls._OUTPUT_STREAM.set_generator_callback(callback)
        return cls._OUTPUT_STREAM


if __name__ == '__main__':

    from src.unit_test.measurement_test_app import test_measurement
    from src.unit_test.measurement_test_class import MeasurementBaseTest

    test_measurement(MeasurementBaseTest)
