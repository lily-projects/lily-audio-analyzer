"""
Grab audio signals and output to a queue.
"""

import copy
import numpy
import threading

from src.models.input_stream import InputStream


class Grabber(object):

    _DEFAULT_MAX_SAMPLES = 5000

    def __init__(self):
        self._max_samples = self._DEFAULT_MAX_SAMPLES
        self._device = None
        self._input_stream = None
        self._sample_data = None
        self._lock = threading.RLock()

    ###########
    # Private #
    ###########

    def _grab(self, input_data, _n_frames, _time_info, _status):
        try:
            self._lock.acquire()

            input_data = numpy.array(list(zip(*input_data)))
            if self._sample_data is None:
                self._sample_data = input_data
            else:
                self._sample_data = numpy.append(self._sample_data, input_data,
                                                 self._sample_data.ndim - 1)

            self._sample_data = self._sample_data[:, -self._max_samples:]

        except (Exception, ):
            self.stop()

        finally:
            self._lock.release()

    ##########
    # Public #
    ##########

    def get_device(self):
        return self._device

    def set_device(self, device_object):
        self._device = device_object

    def set_max_samples(self, max_samples):
        try:
            self._lock.acquire()
            self._max_samples = max_samples
            if self._sample_data is not None:
                self._sample_data = self._sample_data[:, -self._max_samples:]
        finally:
            self._lock.release()

    def start(self):
        if self._device is not None:
            self._input_stream = InputStream(self._device, self._grab)
            self._input_stream.start()

    def is_running(self):
        return self._input_stream is not None and self._input_stream.is_active()

    def stop(self):
        if self.is_running():
            self._input_stream.stop()

    def get_data(self):
        try:
            self._lock.acquire()
            data = copy.deepcopy(self._sample_data)

        finally:
            self._lock.release()

        return data


if __name__ == '__main__':

    import time

    from devices import select_input_device

    device = select_input_device(True)
    if device is None:
        exit(1)

    grab = Grabber()
    grab.set_device(device)
    grab.set_max_samples(1000)

    print('Grab signal from:')
    print(grab.get_device())

    grab.start()
    print('\nIs running:', grab.is_running())

    time.sleep(1)

    grab.stop()
    print('\nIs running:', grab.is_running())

    print('\nGrabbed data:')
    grabbed_data = grab.get_data()
    for i, channel_data in enumerate(grabbed_data):
        print('CH {}, [{}], {}'.format(i + 1, len(channel_data),
                                       ' '.join(map(lambda x: '{: .6f}'.format(x), channel_data[:12]))))
