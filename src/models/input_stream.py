"""
Input stream object.
Can be a sound device stream or a simulator stream.
"""

import sounddevice

from src.models.device_simulator import DeviceSimulator


class InputStream(object):

    def __init__(self, device, callback_function):
        device_index = device.get_index()
        if device_index == DeviceSimulator.INDEX:
            self._input_stream = DeviceSimulator.get_input_stream(callback_function)
        else:
            sample_rate = device.get_default_sample_rate()
            self._input_stream = sounddevice.InputStream(device=device_index,
                                                         samplerate=sample_rate,
                                                         callback=callback_function)

    def start(self): self._input_stream.start()
    def stop(self): self._input_stream.stop()
    def is_active(self): return self._input_stream.active


if __name__ == '__main__':

    import time

    from devices import select_input_device

    def _callback_function(*params):
        print(params)


    test_device = select_input_device(True)
    if test_device is None:
        exit(1)

    input_stream = InputStream(test_device, _callback_function)
    input_stream.start()
    time.sleep(1)
    print('Is active:', input_stream.is_active())
    input_stream.stop()
    print('Is active:', input_stream.is_active())
