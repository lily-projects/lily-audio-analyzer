"""
Device model representing a sound device
"""

import sounddevice


class Device(object):

    DEVICE_TYPE_INPUT = 'input'
    DEVICE_TYPE_OUTPUT = 'output'

    def __init__(self, device_data):
        self._index = device_data['index']
        self._name = device_data['name']
        self._host_api = sounddevice.query_hostapis(device_data['hostapi'])['name']
        self._max_input_channels = device_data['max_input_channels']
        self._max_output_channels = device_data['max_output_channels']
        self._default_sample_rate = device_data['default_samplerate']
        self._default_max_latency = 0
        if self._max_input_channels > 0:
            self._default_max_latency = device_data['default_high_input_latency']
        elif self._max_output_channels > 0:
            self._default_max_latency = device_data['default_high_output_latency']
        self._simulator_function = None
        if 'simulator_function' in device_data.keys():
            self._simulator_function = device_data['simulator_function']

    def __str__(self):
        output = 'Device info:\n'
        output += 'Index              : {value}\n'.format(value=self.get_index())
        output += 'Name               : {value}\n'.format(value=self.get_name())
        output += 'Host API           : {value}\n'.format(value=self.get_host_api())
        output += 'Max input channels : {value}\n'.format(value=self.get_max_input_channels())
        output += 'Max output channels: {value}\n'.format(value=self.get_max_output_channels())
        output += 'Default sample rate: {value}\n'.format(value=self.get_default_sample_rate())
        output += 'Default max latency: {value}'.format(value=self.get_default_max_latency())
        if self.get_simulator_function() is not None:
            output += '\nSimulator function : {value}'.format(value=self.get_simulator_function())
        return output

    def get_index(self): return self._index
    def get_name(self): return self._name
    def get_host_api(self): return self._host_api
    def get_max_input_channels(self): return self._max_input_channels
    def get_max_output_channels(self): return self._max_output_channels
    def get_default_sample_rate(self): return self._default_sample_rate
    def get_default_max_latency(self): return self._default_max_latency
    def get_simulator_function(self): return self._simulator_function


if __name__ == '__main__':

    device = Device(sounddevice.query_devices()[0])
    print(device)

    print('\nDone')
