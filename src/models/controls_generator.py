"""
Generate controls for setting values.
"""

import wx


_LABEL_WIDTH = 115
_MIN_WIDTH = 70


def generate_controls(parent, label, wx_control, **options):
    width = options.get('width', _MIN_WIDTH)
    label = wx.StaticText(parent, wx.ID_ANY, label + ':', size=(_LABEL_WIDTH, -1))
    if wx_control == wx.ComboBox:
        control = wx_control(parent, wx.ID_ANY, style=wx.CB_READONLY, size=(width, -1), choices=options['choices'])
        control.SetValue(options['default'])
    elif wx_control == wx.SpinCtrl:
        control = wx_control(parent, wx.ID_ANY, size=(width, -1), min=options['min'], max=options['max'],
                             initial=options['default'])
    elif wx_control == wx.CheckBox:
        control = wx_control(parent, wx.ID_ANY)
        control.SetValue(options['default'])
    elif wx_control == wx.TextCtrl:
        control = wx_control(parent, wx.ID_ANY, options['default'], size=(width, -1))
    else:
        control = wx.StaticText(parent, wx.ID_ANY, 'no match for {}'.format(wx_control))

    return label, control


if __name__ == '__main__':

    def _add_to_grid(label, control):
        global row
        grid.Add(lbl, (row, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        grid.Add(ctrl, (row, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        row += 1


    app = wx.App(redirect=False)

    frame = wx.Dialog(None, wx.ID_ANY, 'Generate controls test', size=(300, 200))
    panel = wx.Panel(frame, wx.ID_ANY)

    row = 0
    grid = wx.GridBagSizer(5, 5)

    lbl, ctrl = generate_controls(panel, 'Max amplitude [dBV]', wx.ComboBox, choices=['ham', 'harmonics',],
                                  width=100, default='spam')
    _add_to_grid(lbl, ctrl)

    lbl, ctrl = generate_controls(panel, 'Start frequency [Hz]', wx.SpinCtrl, min=1, max=100000, default=100000)
    _add_to_grid(lbl, ctrl)

    lbl, ctrl = generate_controls(panel, 'Logarithmic scale', wx.CheckBox, default=True)
    _add_to_grid(lbl, ctrl)

    lbl, ctrl = generate_controls(panel, 'Sense resistor [Ohm]', wx.TextCtrl, default="spam")
    _add_to_grid(lbl, ctrl)

    box = wx.BoxSizer(wx.HORIZONTAL)
    box.Add(grid, 0, wx.EXPAND | wx.ALL, 10)

    panel.SetSizer(box)

    frame.ShowModal()
    frame.Destroy()
