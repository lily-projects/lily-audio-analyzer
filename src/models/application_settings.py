"""
Application settings model.
"""

import json
import os.path

from src.app_init import EXE_NAME
from src.app_init import USER_FOLDER


class ApplicationSettings(object):

    def __init__(self, logger):
        self._logger = logger
        self._filename = os.path.join(USER_FOLDER, '%s.json' % EXE_NAME)

    ###########
    # Private #
    ###########

    def _get_settings(self):
        try:
            return json.load(open(self._filename, 'r'))
        except FileNotFoundError:
            pass
        except Exception as e:
            self._logger.error('Error reading file: {}'.format(self._filename))
            self._logger.error(str(e))
            self._logger.error('Using default settings')

        return {}

    def _store_settings(self, settings):
        try:
            json.dump(settings, open(self._filename, 'w'), indent=2)
        except Exception as e:
            self._logger.error('Error writing file: {}'.format(self._filename))
            self._logger.error(e)

    def _get_value(self, main_key, sub_key, default_value):
        value = default_value
        settings = self._get_settings()
        try:
            value = settings[main_key][sub_key]
        except KeyError:
            pass

        return value

    def _store_value(self, main_key, sub_key, value):
        settings = self._get_settings()
        if main_key not in settings.keys():
            settings[main_key] = {}
        settings[main_key][sub_key] = value
        self._store_settings(settings)

    #################################
    # Main window position and size #
    #################################

    def get_main_window_position(self): return self._get_value('main_window', 'position', [-1, -1])
    def store_main_window_position(self, value): self._store_value('main_window', 'position', value)
    def get_main_window_size(self): return self._get_value('main_window', 'size', [-1, -1])
    def store_main_window_size(self, value): self._store_value('main_window', 'size', value)
    def get_main_window_maximized(self): return self._get_value('main_window', 'maximized', False)
    def store_main_window_maximized(self, value): self._store_value('main_window', 'maximized', value)

    #########################
    # Sound device settings #
    #########################

    def get_input_device_name(self): return self._get_value('devices', 'input_device_name', '')
    def store_input_device_name(self, value): self._store_value('devices', 'input_device_name', value)
    def get_output_device_name(self): return self._get_value('devices', 'output_device_name', '')
    def store_output_device_name(self, value): self._store_value('devices', 'output_device_name', value)

    #################################
    # Log window position and size #
    #################################

    def get_log_window_position(self): return self._get_value('log_window', 'position', [-1, -1])
    def store_log_window_position(self, value): self._store_value('log_window', 'position', value)
    def get_log_window_size(self): return self._get_value('log_window', 'size', [-1, -1])
    def store_log_window_size(self, value): self._store_value('log_window', 'size', value)
    def get_log_window_maximized(self): return self._get_value('log_window', 'maximized', False)
    def store_log_window_maximized(self, value): self._store_value('log_window', 'maximized', value)
    def get_log_window_auto_open(self): return self._get_value('log_window', 'auto_open', False)
    def store_log_window_auto_open(self, value): self._store_value('log_window', 'auto_open', value)

    ########################
    # Reload configuration #
    ########################

    def get_reload_configuration_enabled(self): return self._get_value('reload_configuration', 'enabled', False)
    def store_reload_configuration_enabled(self, value): self._store_value('reload_configuration', 'enabled', value)
    def get_reload_configuration_filename(self): return self._get_value('reload_configuration', 'filename', '')
    def store_reload_configuration_filename(self, value): self._store_value('reload_configuration', 'filename', value)

    #################
    # Add simulator #
    #################

    def get_add_simulator_devices(self): return self._get_value('debug', 'add_simulator', False)
    def store_add_simulator_devices(self, value): self._store_value('debug', 'add_simulator', value)


if __name__ == '__main__':

    from src.models.logger import Logger

    test_logger = Logger(True)

    app_settings = ApplicationSettings(test_logger)

    print('Main window position:')
    current_value = app_settings.get_main_window_position()
    print(current_value)
    new_value = (current_value[0] + 10, current_value[1] + 10)
    app_settings.store_main_window_position(new_value)
    current_value = app_settings.get_main_window_position()
    print(current_value)

    print('\nMain window size:')
    current_value = app_settings.get_main_window_size()
    print(current_value)
    new_value = (current_value[0] + 10, current_value[1] + 10)
    app_settings.store_main_window_size(new_value)
    current_value = app_settings.get_main_window_size()
    print(current_value)

    print('\nMain window maximized:')
    current_value = app_settings.get_main_window_maximized()
    print(current_value)
    new_value = not current_value
    app_settings.store_main_window_maximized(new_value)
    current_value = app_settings.get_main_window_maximized()
    print(current_value)
    app_settings.store_main_window_maximized(False)

    print('\nInput device name:')
    current_value = app_settings.get_input_device_name()
    print(current_value)
    new_value = 'ham' if current_value == 'spam' else 'spam'
    app_settings.store_input_device_name(new_value)
    current_value = app_settings.get_input_device_name()
    print(current_value)

    print('\nOutput device name:')
    current_value = app_settings.get_output_device_name()
    print(current_value)
    new_value = 'ham' if current_value == 'spam' else 'spam'
    app_settings.store_output_device_name(new_value)
    current_value = app_settings.get_output_device_name()
    print(current_value)

    print('Log window position:')
    current_value = app_settings.get_log_window_position()
    print(current_value)
    new_value = (current_value[0] + 10, current_value[1] + 10)
    app_settings.store_log_window_position(new_value)
    current_value = app_settings.get_log_window_position()
    print(current_value)

    print('\nLog window size:')
    current_value = app_settings.get_log_window_size()
    print(current_value)
    new_value = (current_value[0] + 10, current_value[1] + 10)
    app_settings.store_log_window_size(new_value)
    current_value = app_settings.get_log_window_size()
    print(current_value)

    print('\nLog window maximized:')
    current_value = app_settings.get_log_window_maximized()
    print(current_value)
    new_value = not current_value
    app_settings.store_log_window_maximized(new_value)
    current_value = app_settings.get_log_window_maximized()
    print(current_value)
    app_settings.store_log_window_maximized(False)

    print('\nLog window auto open:')
    current_value = app_settings.get_log_window_auto_open()
    print(current_value)
    new_value = not current_value
    app_settings.store_log_window_auto_open(new_value)
    current_value = app_settings.get_log_window_auto_open()
    print(current_value)
    app_settings.store_log_window_auto_open(False)

    print('\nReload configuration enabled:')
    current_value = app_settings.get_reload_configuration_enabled()
    print(current_value)
    new_value = not current_value
    app_settings.store_reload_configuration_enabled(new_value)
    current_value = app_settings.get_reload_configuration_enabled()
    print(current_value)
    app_settings.store_reload_configuration_enabled(False)

    print('\nReload configuration filename:')
    current_value = app_settings.get_reload_configuration_filename()
    print(current_value)
    new_value = 'ham' if current_value != 'ham' else 'spam'
    app_settings.store_reload_configuration_filename(new_value)
    current_value = app_settings.get_reload_configuration_filename()
    print(current_value)

    print('\nAdd simulator:')
    current_value = app_settings.get_add_simulator_devices()
    print(current_value)
    new_value = not current_value
    app_settings.store_add_simulator_devices(new_value)
    current_value = app_settings.get_add_simulator_devices()
    print(current_value)
    app_settings.store_add_simulator_devices(False)
