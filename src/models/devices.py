""""
List all available devices and create device models of each device
"""

import sounddevice

from src.models.device import Device
from src.models.device_simulator import DeviceSimulator


###########
# Private #
###########

def _get_host_api_name(api_index): return sounddevice.query_hostapis(api_index)['name']


def _create_list_of_device_names(device_type, add_simulator):
    devices = filter(lambda x: (device_type == Device.DEVICE_TYPE_OUTPUT and x['max_output_channels'] > 0) or
                               (device_type == Device.DEVICE_TYPE_INPUT and x['max_input_channels'] > 0),
                     sounddevice.query_devices())

    devices = list(
        map(lambda x: '{}, {} channels, {}Hz, {}'.format(
            x['name'], (x['max_input_channels'] + x['max_output_channels']),
            int(x['default_samplerate']), _get_host_api_name(x['hostapi'])
        ), devices)
    )

    devices.sort()

    if add_simulator:
        devices.extend(DeviceSimulator.get_device_names(device_type))

    return devices


def _select_device(device_type, add_simulator):
    print('Select %s device:' % device_type)
    device_names = _create_list_of_device_names(device_type, add_simulator)
    for i, device_name in enumerate(device_names):
        print('%2d - %s' % ((i + 1), device_name))
    index = input('Enter a number: ')
    try:
        device_name = device_names[int(index) - 1]
        device = get_device_from_name(device_name, device_type)
    except (Exception, ):
        device = None

    return device


##########
# Public #
##########

def get_list_of_output_device_names(add_simulator):
    return _create_list_of_device_names(Device.DEVICE_TYPE_OUTPUT, add_simulator)


def get_list_of_input_device_names(add_simulator):
    return _create_list_of_device_names(Device.DEVICE_TYPE_INPUT, add_simulator)


def get_device_from_name(device_name, device_type):
    device = None

    parts = list(map(lambda x: x.strip(), device_name.split(',')))
    parts[1] = int(parts[1].split(' ')[0])
    parts[2] = float(parts[2].replace('Hz', ''))
    matches = list(filter(lambda x:
                          x['name'] == parts[0] and
                          ((device_type == Device.DEVICE_TYPE_INPUT and x['max_input_channels'] == parts[1]) or
                           (device_type == Device.DEVICE_TYPE_OUTPUT and x['max_output_channels'] == parts[1])) and
                          x['default_samplerate'] == parts[2] and
                          _get_host_api_name(x['hostapi']) == parts[3],
                          sounddevice.query_devices()))

    if len(matches) == 1:
        device = Device(matches[0])

    if len(matches) == 0:
        device = DeviceSimulator.get_device_from_name(device_name)

    return device


def select_output_device(add_simulator):
    return _select_device(Device.DEVICE_TYPE_OUTPUT, add_simulator)


def select_input_device(add_simulator):
    return _select_device(Device.DEVICE_TYPE_INPUT, add_simulator)


if __name__ == '__main__':

    print('Output devices:')
    for output_device_name in get_list_of_output_device_names(True):
        print(output_device_name)

    print('\nInput devices:')
    for input_device_name in get_list_of_input_device_names(True):
        print(input_device_name)

    print()
    print(select_output_device(True))

    print()
    print(select_input_device(True))
