"""
Configuration model.
"""

import json


class Configuration(object):

    _KEY_MEASUREMENTS = 'measurements'

    def __init__(self):
        self._configuration = {}
        self._filename = 'new configuration'
        self._has_changes = False

    def __str__(self):
        output = 'Filename: {value}\n'.format(value=self.get_filename())
        output += 'Measurements:\n'
        for measurement_name in self.get_measurement_names():
            output += '  - Name: {value}\n'.format(value=measurement_name)
            output += '  - Settings:\n'
            settings = self.get_measurement_settings(measurement_name)
            for key in settings.keys():
                output += '    - {key}: {value}\n'.format(key=key, value=settings[key])

        return output

    ##########
    # Public #
    ##########

    def get_filename(self):
        return self._filename

    def has_changes(self):
        return self._has_changes

    def get_measurement_names(self):
        return sorted(self._configuration.get(self._KEY_MEASUREMENTS, {}).keys())

    def update_measurement_name(self, old_measurement_name, new_measurement_name):
        measurements = self._configuration.get(self._KEY_MEASUREMENTS, {})
        measurements[new_measurement_name] = measurements.pop(old_measurement_name)
        self._configuration[self._KEY_MEASUREMENTS] = measurements
        self._has_changes = True

    def update_measurement(self, measurement_name, settings):
        if self._KEY_MEASUREMENTS not in self._configuration.keys():
            self._configuration[self._KEY_MEASUREMENTS] = {}
        self._configuration[self._KEY_MEASUREMENTS][measurement_name] = settings
        self._has_changes = True

    def get_measurement_settings(self, measurement_name):
        return self._configuration.get(self._KEY_MEASUREMENTS, {}).get(measurement_name, {})

    def delete_measurement(self, measurement_name):
        self._configuration[self._KEY_MEASUREMENTS].pop(measurement_name)
        self._has_changes = True

    def save_to_file(self, filename):
        json.dump(self._configuration, open(filename, 'w'), indent=4)
        self._filename = filename
        self._has_changes = False

    def load_from_file(self, filename):
        self._configuration = json.load(open(filename, 'r'))
        self._filename = filename
        self._has_changes = False


if __name__ == '__main__':

    import os

    test_filename = os.path.join(os.path.expanduser('~'), 'test.json')

    config = Configuration()

    print(config)

    config.update_measurement('bacon', {'type': 'ham', 'taste': 'spam'})

    print(config)

    config.update_measurement('bacon', {'type': 'ham', 'taste': 'eggs'})

    print(config)

    config.update_measurement_name('bacon', 'spam')

    print(config)

    config.update_measurement('bacon', {'type': 'ham', 'taste': 'eggs'})
    config.delete_measurement('spam')

    print(config)

    print('Save to file . . .')
    config.save_to_file(test_filename)
    print(config)

    print('Empty config')
    config = Configuration()
    print(config)

    print('Load from file . . .')
    config.load_from_file(test_filename)
    print(config)

    if os.path.isfile(test_filename):
        os.remove(test_filename)
