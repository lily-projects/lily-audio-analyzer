"""
Logger for the application.
"""

import os
import sys
import threading
import time

from src.app_init import EXE_NAME
from src.app_init import USER_FOLDER
from src.models.string_formatter import get_timestamp


class Logger(object):

    TYPE_INFO = 'INFO'
    TYPE_DEBUG = 'DEBUG'
    TYPE_ERROR = 'ERROR'
    TYPE_STDOUT = 'STDOUT'
    TYPE_STDERR = 'STDERR'

    _LOG_FORMAT = '{} - {:6} - {}\n'

    _FILENAME = os.path.join(USER_FOLDER, '{}.log'.format(EXE_NAME))

    class _StdLogger(object):

        def __init__(self, logger, std_type):
            self._logger = logger
            self._type = std_type

        def write(self, message):
            self._logger.handle_message(self._type, message)

        def flush(self):
            pass

    def __init__(self, log_to_stdout=False):
        self._log_to_stdout = log_to_stdout
        open(self._FILENAME, 'w').close()
        self._output = ''

        self._orgStdout = sys.stdout
        self._orgStderr = sys.stderr
        sys.stdout = self._StdLogger(self, self.TYPE_STDOUT)
        sys.stderr = self._StdLogger(self, self.TYPE_STDERR)

    @classmethod
    def get_filename(cls):
        return cls._FILENAME

    def shut_down(self):
        sys.stdout = self._orgStdout
        sys.stderr = self._orgStderr

    def info(self, message):
        self.handle_message(self.TYPE_INFO, '{}\n'.format(message))

    def debug(self, message):
        self.handle_message(self.TYPE_DEBUG, '{}\n'.format(message))

    def error(self, message):
        self.handle_message(self.TYPE_ERROR, '{}\n'.format(message))

    def handle_message(self, message_type, message_text):
        timestamp = get_timestamp()

        self._output += message_text
        while '\n' in self._output:
            index = self._output.find('\n')
            line = self._LOG_FORMAT.format(timestamp, message_type, self._output[:index])
            self._output = self._output[index + 1:]

            with open(self._FILENAME, 'a') as fp:
                fp.write(line)
            if self._log_to_stdout:
                self._orgStdout.write(line)


if __name__ == "__main__":

    def _generate_error():
        def _exception(): _dummy = 1 / 0

        t = threading.Thread(target=_exception)
        t.start()
        time.sleep(1)


    test_logger = Logger(True)
    test_logger.info('This is an info message.')
    test_logger.debug('This is a debug message.')
    test_logger.error('This is an error message.')

    print('This is a stdout message.')
    print('This is a\nmulti line message.')

    _generate_error()

    test_logger.shut_down()

    with open(test_logger.get_filename(), 'r') as tfp:
        content = tfp.read()

    print('\nLog file content:')
    print(content)
