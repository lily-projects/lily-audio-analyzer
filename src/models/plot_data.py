"""
Defines the plot data object
"""


class PlotData(object):

    def __init__(self):
        self._x_data = []
        self._x_log_scale = False
        self._y_data = []
        self._y_min = -1
        self._y_max = 1
        self._x_legend = ''
        self._y_legend = ''
        self._properties = []

    def __str__(self):
        channel_count = 0
        if len(self.get_y_data()) > 0:
            channel_count = len(self.get_y_data()[0])

        output = [
            'X data count   : {}'.format(len(self.get_x_data())),
            'X log scale    : {}'.format(self.get_x_log_scale()),
            'Y data channels: {}'.format(len(self.get_y_data())),
            'Channel count  : {}'.format(channel_count),
            'Y min          : {}'.format(self.get_y_min()),
            'Y max          : {}'.format(self.get_y_max()),
            'X legend       : {}'.format(self.get_x_legend()),
            'Y legend       : {}'.format(self.get_y_legend()),
        ]
        if len(self.get_plot_properties()) == 0:
            output.append('No properties')
        else:
            output.append('Properties:')
            for i, plot_property in enumerate(self.get_plot_properties()):
                output.append('Property: {}'.format(i + 1))
                output.append(str(plot_property))

        return '\n'.join(output)

    ##########
    # Public #
    ##########

    def get_x_data(self): return self._x_data
    def set_x_data(self, data): self._x_data = data

    def get_x_log_scale(self): return self._x_log_scale
    def set_x_log_scale(self, value): self._x_log_scale = value

    def get_y_data(self): return self._y_data
    def set_y_data(self, data): self._y_data = data

    def get_y_min(self): return self._y_min
    def set_y_min(self, value): self._y_min = value

    def get_y_max(self): return self._y_max
    def set_y_max(self, value): self._y_max = value

    def get_x_legend(self): return self._x_legend
    def set_x_legend(self, value): self._x_legend = value

    def get_y_legend(self): return self._y_legend
    def set_y_legend(self, value): self._y_legend = value

    def get_plot_properties(self): return self._properties

    def update_plot_property(self, group, label, value):
        matches = list(filter(lambda x: x.get_group().lower() == group.lower() and
                                        x.get_label().lower() == label.lower(), self._properties))
        if len(matches) == 0:
            self._properties.append(PlotProperty(group, label, value))
        else:
            matches[0].set_value(value)


class PlotProperty(object):

    def __init__(self, group, label, value):
        self._group = group
        self._label = label
        self._value = value

    def __str__(self):
        output = [
            ' - Group: {}'.format(self.get_group()),
            ' - Label: {}'.format(self.get_label()),
            ' - Value: {}'.format(self.get_value())
        ]
        return '\n'.join(output)

    ##########
    # Public #
    ##########

    def get_group(self): return self._group
    def get_label(self): return self._label
    def get_value(self): return self._value
    def set_value(self, value): self._value = value


if __name__ == '__main__':
    plot_data = PlotData()
    print(plot_data)

    plot_data.set_x_data([x for x in range(1000)])
    plot_data.set_y_data([[x for x in range(1000)], [x for x in range(1000)]])
    plot_data.set_y_min(-10)
    plot_data.set_y_max(10)
    plot_data.set_x_legend('Frequency [Hz]')
    plot_data.set_y_legend('Gain [dB]')
    plot_data.update_plot_property('general', 'time', 0.2)
    plot_data.update_plot_property('General', 'Time', 0.2)
    print(plot_data)

    plot_data.update_plot_property('general', 'time', 0.5)
    print(plot_data)
