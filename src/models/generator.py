"""
Generate signals on an output of a device.
"""

import math
import numpy
import scipy

from src.models.output_stream import OutputStream


class Generator(object):

    WAVE_SINE = 'sine'
    WAVE_TRIANGLE = 'triangle'
    WAVE_SQUARE = 'square'
    WAVE_HARMONICS = 'harmonics'

    DEFAULT_WAVE = WAVE_SINE
    DEFAULT_FREQUENCY = 1000
    DEFAULT_AMPLITUDE = 0.5

    def __init__(self):
        self._device = None
        self._output_stream = None
        self._sample_index = 0
        self._wave_form = self.DEFAULT_WAVE
        self._current_frequency = self.DEFAULT_FREQUENCY
        self._new_frequency = self.DEFAULT_FREQUENCY
        self._amplitude = self.DEFAULT_AMPLITUDE
        self._wave_generator = {
            self.WAVE_SINE: self._generate_sine,
            self.WAVE_TRIANGLE: self._generate_triangle,
            self.WAVE_SQUARE: self._generate_square,
            self.WAVE_HARMONICS: self._generate_harmonics
        }

    ###########
    # Private #
    ###########

    def _generate_sine(self, t_values):
        return self._amplitude * numpy.sin(2 * numpy.pi * self._current_frequency * t_values)

    def _generate_triangle(self, t_values):
        return self._amplitude * scipy.signal.sawtooth(2 * numpy.pi * self._current_frequency * t_values, 0.5)

    def _generate_square(self, t_values):
        return self._amplitude * scipy.signal.square(2 * numpy.pi * self._current_frequency * t_values, 0.5)

    def _generate_harmonics(self, t_values):
        samples = self._generate_sine(t_values)
        sample_rate = self._device.get_default_sample_rate()
        frequency = self._current_frequency * 2
        amplitude = self._amplitude / math.sqrt(2)
        while frequency < sample_rate / 2:
            samples += amplitude * numpy.sin(2 * numpy.pi * frequency * t_values)
            frequency += self._current_frequency
            amplitude /= math.sqrt(2)

        return samples

    def _generator(self, output_data, n_frames, _time_info, _status):
        try:
            sample_rate = self._device.get_default_sample_rate()
            n_channels = self._device.get_max_output_channels()

            # Make fluent frequency changes on complete periods
            n_samples_per_period = sample_rate / self._current_frequency
            next_period_start = round(math.ceil(self._sample_index / n_samples_per_period) * n_samples_per_period)

            if self._new_frequency != self._current_frequency and next_period_start < self._sample_index + n_frames:
                # Change frequency at next period
                n_current_samples = next_period_start - self._sample_index
                n_new_samples = n_frames - n_current_samples

                current_data = None
                if n_current_samples > 0:
                    # Finish current frequency
                    t_values = (self._sample_index + numpy.arange(n_current_samples)) / sample_rate
                    t_values = t_values.reshape((-1, 1)).repeat(n_channels, axis=1)
                    current_data = self._wave_generator[self._wave_form](t_values)

                # Start new frequency
                self._current_frequency = self._new_frequency
                t_values = numpy.arange(n_new_samples) / sample_rate
                t_values = t_values.reshape((-1, 1)).repeat(n_channels, axis=1)
                new_data = self._wave_generator[self._wave_form](t_values)

                if current_data is not None:
                    output_data[:] = numpy.append(current_data, new_data, axis=0)
                else:
                    output_data[:] = new_data

                self._sample_index = n_new_samples

            else:
                t_values = (self._sample_index + numpy.arange(n_frames)) / sample_rate
                t_values = t_values.reshape((-1, 1)).repeat(n_channels, axis=1)
                output_data[:] = self._wave_generator[self._wave_form](t_values)
                self._sample_index += n_frames

        except (Exception, ):
            self.stop()

    def _generate_wave(self):
        if self._device is not None:
            self._sample_index = 0
            self._output_stream = OutputStream(self._device, self._generator)
            self._output_stream.start()

    ##########
    # Public #
    ##########

    def get_available_wave_forms(self):
        return list(self._wave_generator.keys())

    def get_device(self):
        return self._device

    def set_device(self, device_object):
        self._device = device_object

    def set_wave_form(self, wave_form):
        self._wave_form = wave_form

    def set_frequency(self, frequency):
        self._new_frequency = frequency

    def set_amplitude(self, amplitude):
        self._amplitude = amplitude

    def start(self):
        self.stop()
        self._generate_wave()

    def is_running(self):
        return self._output_stream is not None and self._output_stream.is_active()

    def stop(self):
        if self.is_running():
            self._output_stream.stop()


if __name__ == '__main__':

    import time

    from devices import select_output_device

    device = select_output_device(True)
    if device is None:
        exit(1)

    gen = Generator()
    gen.set_device(device)

    print('Generate sine wave on:')
    print(gen.get_device())

    gen.start()
    for wave in (gen.WAVE_SINE, gen.WAVE_TRIANGLE, gen.WAVE_SQUARE):
        gen.set_wave_form(wave)
        time.sleep(3)

    gen.set_wave_form(gen.WAVE_SINE)
    for test_frequency in (2000, 1000):
        time.sleep(1)
        gen.set_frequency(test_frequency)

    for test_amplitude in (1, 0.5):
        time.sleep(1)
        gen.set_amplitude(test_amplitude)

    # Frequency changes should be without plops
    for test_frequency in (217, 253, 296, 345, 402, 469, 547):
        time.sleep(0.3)
        gen.set_frequency(test_frequency)

    time.sleep(1)
    gen.stop()
