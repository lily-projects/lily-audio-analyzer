"""
Widget with generator controls.
"""

import wx

from src.models.generator import Generator
from src.models.controls_generator import generate_controls


class GeneratorControls(wx.StaticBoxSizer):

    SHOW_WAVE_FORM = 'show_wave_form'
    SHOW_FREQUENCY = 'show_frequency'
    SHOW_AMPLITUDE = 'show_amplitude'
    SHOW_OUTPUT_ENABLE = 'show_output_enable'

    SHOW_ALL = [
        SHOW_WAVE_FORM,
        SHOW_FREQUENCY,
        SHOW_AMPLITUDE,
        SHOW_OUTPUT_ENABLE
    ]

    _BOX_CAPTION = 'Generator'
    _CONTROLS_SPACING = 5

    def __init__(self, parent, view_options, add_control_callback):
        super().__init__(wx.StaticBox(parent, wx.ID_ANY, ' {}: '.format(self._BOX_CAPTION)), wx.VERTICAL)

        self._row = 0
        self._grid = wx.GridBagSizer(self._CONTROLS_SPACING, self._CONTROLS_SPACING)

        if self.SHOW_WAVE_FORM in view_options:
            label, control = generate_controls(parent, 'Wave form', wx.ComboBox,
                                               choices=Generator().get_available_wave_forms(),
                                               width=90,
                                               default=Generator.WAVE_SINE)
            self._add_to_grid(label, control)
            add_control_callback('wave_form', str, control, Generator.WAVE_SINE)

        if self.SHOW_FREQUENCY in view_options:
            label, control = generate_controls(parent, 'Frequency [Hz]', wx.SpinCtrl,
                                               min=1, max=100000,
                                               default=Generator.DEFAULT_FREQUENCY)
            self._add_to_grid(label, control)
            add_control_callback('frequency', int, control, Generator.DEFAULT_FREQUENCY)

        if self.SHOW_AMPLITUDE in view_options:
            label, control = generate_controls(parent, 'Amplitude [%]', wx.SpinCtrl,
                                               min=1, max=100,
                                               default=int(Generator.DEFAULT_AMPLITUDE * 100))
            self._add_to_grid(label, control)
            add_control_callback('amplitude', int, control, int(Generator.DEFAULT_AMPLITUDE * 100))

        if self.SHOW_OUTPUT_ENABLE in view_options:
            label, control = generate_controls(parent, 'Output enable', wx.CheckBox, default=False)
            self._add_to_grid(label, control)
            add_control_callback('output_enable', bool, control, False)

        self.Add(self._grid, 0, wx.EXPAND | wx.ALL, self._CONTROLS_SPACING)

    def _add_to_grid(self, label, control):
        self._grid.Add(label, (self._row, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        self._grid.Add(control, (self._row, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        self._row += 1


if __name__ == '__main__':

    from src.unit_test.view_test_controls import show_view_test_controls

    show_view_test_controls()
