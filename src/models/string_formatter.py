"""
Various string formatting functions
"""

from datetime import datetime


_TIME_STAMP_FORMAT = "%Y%m%d %H:%M:%S"


def get_timestamp():
    return datetime.now().strftime(_TIME_STAMP_FORMAT)


def format_value(value, unit='', decimals=2):
    units = ['G', 'M', 'k', '', 'm', 'u', 'n', 'p', 'f']
    index = 3
    sign = ''

    if value < 0:
        value = abs(value)
        sign = '-'

    if value != 0:
        while value > 1:
            value /= 1000
            index -= 1
            if index == 0:
                break

        while value < 1:
            value *= 1000
            index += 1
            if index == len(units) - 1:
                break

    format_string = '%s%%.%df %s%s' % (sign, decimals, units[index], unit)
    return format_string % value


if __name__ == '__main__':

    print(get_timestamp())

    print('\nPositive values:')
    test_value = 10e12
    end_value = 10e-18
    while test_value > end_value:
        print('%s => %s' % (test_value, format_value(test_value, 'V', 3)))
        test_value /= 1000

    print('\nZero:', format_value(0, 'V', 3))

    print('\nNegative values:')
    test_value = -10e12
    end_value = -10e-18
    while test_value < end_value:
        print('%s => %s' % (test_value, format_value(test_value, 'V', 3)))
        test_value /= 1000
