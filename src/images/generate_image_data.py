"""
Automatically generates a dictionary with data from the images in this folder.
We need this to properly create an executable.

Image source: http://www.iconsdb.com/
Image color : #325c81
"""

import os


image_data = {}

for item in os.listdir('.'):
    if item.endswith('.png'):
        with open('%s' % item, 'rb') as fp:
            image_data[item] = fp.read()

with open('../models/image_data.py', 'w') as fp:
    fp.write('"""\n')
    fp.write('Automatically generated image data for using images in the application.\n')
    fp.write('"""\n\n')
    fp.write('IMAGE_DATA = {\n')
    for i, item in enumerate(image_data.keys()):
        fp.write('    \'%s\': %s' % (item, image_data[item]))
        if i < len(image_data.keys()) - 1:
            fp.write(',')
        fp.write('\n')
    fp.write('}\n')

print('\nDone')
