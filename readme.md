## Lily Audio Analyzer

Application software for audio analysis using the PC soundcard.
For better performance, an external audio device is recommended (like Focusrite, M-Audio, etc.).
The application is completely written in Python.

# Installation

Installation requirements.

* Python 3
* wxPython (pip install wxpython)
* python-sounddevice (pip install sounddevice)
* python scipy (pip install scipy)
