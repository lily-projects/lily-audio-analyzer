"""
Create deployment package, basically a zip file with the executable and the manual and stuff.
"""

import os
import PyInstaller.__main__
import re
import releases
import shutil
import src
import zipfile

from deploy import deployment_output
from src import app_init


def _clean_output_folder(output_folder):
    print("Clean output folder . . .")
    for item in os.listdir(output_folder):
        full_path = os.path.join(output_folder, item)
        if os.path.isfile(full_path) and item != '__init__.py':
            os.remove(full_path)
        elif os.path.isdir(full_path):
            shutil.rmtree(full_path)


def _create_version_file(version_file, version_tuple, version_string):
    version_template = 'app_version.template'

    print('Create version info file . . .')
    with open(version_template, 'r') as fp:
        version_template = fp.read()
        version_template = version_template.replace('{app_name}', app_init.APP_NAME)
        version_template = version_template.replace('{version_tuple}', str(version_tuple))
        version_template = version_template.replace('{version_string}', version_string)
        version_template = version_template.replace('{exe_name}', app_init.EXE_NAME)
        version_template = version_template.replace('{company_name}', app_init.COMPANY)

    with open(version_file, 'w') as fp:
        fp.write(version_template)


def _update_app_init(version_string):
    filename = app_init.__file__
    with open(filename, 'r') as fp:
        content = fp.read()

    match = re.findall(r"VERSION = '\S+'", content)[0]
    content = content.replace(match, "VERSION = '{value}'".format(value=version_string))

    with open(filename, 'w') as fp:
        fp.write(content)


def _create_zip_file(output_folder, zip_filename):
    print('Create ZIP file for distribution . . .')
    zip_filename = os.path.join(output_folder, zip_filename)
    zip_file = zipfile.ZipFile(zip_filename, 'w')
    # Add executable
    item_name = '{exe_name}.exe'.format(exe_name=app_init.EXE_NAME)
    item_path = os.path.join(output_folder, item_name)
    zip_file.write(item_path, item_name)

    print('TODO: add manual when ready.')

    # ZIP ready
    zip_file.close()


def _move_to_release_folder(output_folder, releases_folder, zip_filename):
    print('Move to release folder. . .')
    shutil.move(os.path.join(output_folder, zip_filename), os.path.join(releases_folder, zip_filename))


def _create_release(output_folder, version_tuple, release_name, rc_version, releases_folder):
    init_file = os.path.join(os.path.dirname(src.__file__), 'main.pyw')
    icon_file = os.path.join(os.path.dirname(src.__file__), 'images', 'app.ico')
    version_file = os.path.join(output_folder, 'app.version')

    version_string = app_init.VERSION
    version_tuple[-1] = rc_version
    if rc_version > 0:
        version_string += '.RC{value}'.format(value=rc_version)

    _clean_output_folder(output_folder)
    _create_version_file(version_file, version_tuple, version_string)
    _update_app_init(version_string)

    PyInstaller.__main__.run([
        init_file,
        '--clean',
        '--onefile',
        '--collect-submodules=scipy',
        '--name={name}'.format(name=app_init.EXE_NAME),
        '--icon={filename}'.format(filename=icon_file),
        '--version-file={filename}'.format(filename=version_file),
        '--workpath={folder_name}'.format(folder_name=output_folder),
        '--specpath={folder_name}'.format(folder_name=output_folder),
        '--distpath={folder_name}'.format(folder_name=output_folder)
    ])

    zip_filename = '{release_name}.zip'.format(release_name=release_name)
    _create_zip_file(output_folder, zip_filename)
    _move_to_release_folder(output_folder, releases_folder, zip_filename)


def create_deployment():
    output_folder = os.path.dirname(deployment_output.__file__)
    releases_folder = os.path.dirname(releases.__file__)
    release_name = '{company}_{exe_name}_v{version}'.format(company=app_init.COMPANY, exe_name=app_init.EXE_NAME,
                                                            version=app_init.VERSION)

    rc_version = 0
    for item in os.listdir(releases_folder):
        match = re.findall(r'\w+_rc(\d+).zip', item)
        if len(match) == 1:
            rc = int(match[0])
            if rc > rc_version:
                rc_version = rc
    rc_version += 1

    release_candidate_name = '{release_name}_rc{rc_version}'.format(release_name=release_name, rc_version=rc_version)

    version_tuple = list(map(lambda x: int(x), app_init.VERSION.split('.')))
    while len(version_tuple) < 4:
        version_tuple.append(0)

    print('Create release {name} . . .' .format(name=release_candidate_name))
    _create_release(output_folder, version_tuple, release_candidate_name, rc_version, releases_folder)
    print('Create release {name} . . .' .format(name=release_name))
    _create_release(output_folder, version_tuple, release_name, 0, releases_folder)

    print('\nBuilding done.')
    print('Release candidate name : {value}'.format(value=release_candidate_name))
    print('Release name           : {value}'.format(value=release_name))
    print('Application            : {value}'.format(value=app_init.APP_NAME))
    print('Version                : {value}'.format(value=app_init.VERSION))
    print('Version tuple          : {value}'.format(value=version_tuple))
    print('Exe name               : {value}'.format(value=app_init.EXE_NAME))
    print('Company name           : {value}'.format(value=app_init.COMPANY))


if __name__ == '__main__':

    create_deployment()
